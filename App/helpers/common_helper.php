<?php

/* This function is used to include the CSS and JS (Sanjay Yadav) */
if (!function_exists('add_includes')) {

    function add_includes($type, $files, $options = NULL, $prepend_base_url = TRUE) {
        $ci = &get_instance();
        $includes = array();
        if (is_array($files) and ! empty($files)) {
            foreach ($files as $file) {
                if ($prepend_base_url) {
                    $ci->load->helper('url');
                    $file = base_url() . $file;
                }

                $includes[$type][] = array(
                    'file' => $file,
                    'options' => $options
                );
            }
        } else {
            if ($prepend_base_url) {
                $ci->load->helper('url');
                $file = base_url() . $files;
            }

            $includes[$type][] = array(
                'file' => $file,
                'options' => $options
            );
        }

        return $includes;
    }

}

/* End Of function add_includes  */

if (!function_exists('getActiveMenu')) {

    function getActiveMenu($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mfl == $url && $mthd != 'setting') {
            echo "active-menu";
        }
    }

}

if (!function_exists('getActiveMenuSetting')) {

    function getActiveMenuSetting($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mthd == $url) {
            echo "active-menu";
        }
    }

}

/* End Of function add_includes  */

if (!function_exists('getActiveResultMenu')) {

    function getActiveResultMenu($url) {
        $CI = &get_instance();
        $alias = $CI->input->get('cat', true);
        if ($alias == $url) {
            echo "active-menu";
        }
    }

}

if (!function_exists('getActiveStatus')) {

    function getActiveStatus($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $mfl . '/' . $CI->router->fetch_method();
        if ($mthd == $url) {
            echo "active";
        }
    }

}

if (!function_exists('check_auth')) {

    function check_auth() {
        $CI = &get_instance();
        $CI->load->module("users");
        //if (!$CI->users->_is_admin()) {
        if (!$CI->users->userdata()) {
            redirect('');
        }
    }

}

if (!function_exists('site_title')) {

    function site_title() {
        $CI = &get_instance();
        $CI->load->module("cms");
        $title = $CI->cms->getSitetitle();
        $title = @unserialize($title);
        return $title['c_title'];
    }

}
if (!function_exists('site_logo')) {

    function site_logo() {
        $CI = &get_instance();
        $CI->db->where('alias', 'logo');
        $query = $CI->db->get('cms');
        return $query->row()->content;
    }

}
if (!function_exists('state_name')) {

    function state_name($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('state');
        if ($query->num_rows() > 0)
            return $query->row()->state_name;
    }

}
if (!function_exists('country_name')) {

    function country_name($id) {
        $CI = &get_instance();
        $CI->db->where('country_code', $id);
        $query = $CI->db->get('country');
        if ($query->num_rows() > 0)
            return $query->row()->country_name;
    }

}


if (!function_exists('getGoogleAnalyticCode')) {

    function getGoogleAnalyticCode($alias) {
        $CI = &get_instance();
        $CI->db->start_cache();
        $CI->db->where('alias', $alias);
        $query = $CI->db->get('cms');
        $CI->db->flush_cache();
        $qqq = $query->row();
        $yourCode = array();
        $yourCode['content'] = unserialize($qqq->content);
        return $yourCode['content']['description'];
    }

}

if (!function_exists('getPages')) {

    function getPages($slug = null) {
        $CI = &get_instance();
        $CI->db->start_cache();
        $con = array(
            'include_in' => $slug,
            'is_active' => '1',
            'is_deleted' => '0'
        );
        $CI->db->where($con);
        $query = $CI->db->get('pages');
        $CI->db->flush_cache();
        $qqq = $query->result();
        return $qqq;
    }

}

if (!function_exists('state_codes')) {

    function state_codes() {
        $CI = &get_instance();
        $query = $CI->db->get('state');
        $states = $query->result();
        $data = array();
        foreach ($states as $list) {
            $data[strtolower($list->state_name)] = $list->id;
        }
        return $data;
    }

}

if (!function_exists('getActiveCollapse')) {

    function getActiveCollapse($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mfl == $url && $mthd != 'setting') {
            echo "in Active";
        }
    }

}

if (!function_exists('getActiveCollapseSetting')) {

    function getActiveCollapseSetting($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mthd == 'setting') {
            echo "in Active";
        }
    }

}

if (!function_exists('site_limit')) {

    function site_limit() {
        $CI = &get_instance();
        $CI->load->module("cms");
        $title = $CI->cms->getSitetitle();
        $title = @unserialize($title);
        return $title['c_limit'];
    }

}

if (!function_exists('industry_list')) {

    function industry_list() {
        $CI = &get_instance();
        $CI->db->where(array('indus_id' => '0', 'is_active' => '1'));
        $query = $CI->db->get('nc_industry');
        $data = array();
        foreach ($query->result() as $list) {
            $data[$list->id] = $list->name;
        }
        return $data;
    }

}
if (!function_exists('functions_list')) {

    function functions_list($id) {
        $CI = &get_instance();
        $CI->db->where(array('indus_id' => $id, 'is_active' => '1'));
        $query = $CI->db->get('nc_industry');
        return $query->result();
    }

}

if (!function_exists('validate_user_interview')) {

    function validate_user_interview($id) {
        $CI = &get_instance();
        $CI->db->where('user_id', $id);
        $CI->db->select_max('interview_id');
        $query = $CI->db->get('nc_interview');
        if ($query->num_rows() > 0) {
            return $query->row()->interview_id;
        } else {
            return false;
        }
    }

}

if (!function_exists('validate_user_assessment_interview')) {

    function validate_user_assessment_interview($id) {
        $CI = &get_instance();
        $CI->db->where('user_id', $id);
        $CI->db->select_max('interview_id');
        $query = $CI->db->get('nc_assessment_interview');
        if ($query->num_rows() > 0) {
            return $query->row()->interview_id;
        } else {
            return false;
        }
    }

}

if (!function_exists('getForumUser')) {

    function getForumUser($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->row()->user_name;
        } else {
            return false;
        }
    }

}

if (!function_exists('getCategoryName')) {

    function getCategoryName() {
        $CI = &get_instance();
        $query = $CI->db->get('event_categories');
        $cats = array();
        foreach ($query->result_array() as $list) {
            $cats[$list['cat_id']] = $list['cat_name'];
        }
        return $cats;
    }

}

if (!function_exists('getUserDeviceIDs')) {

    function getUserDeviceIDs($userID = '', $deviceType = '') {
        $CI = &get_instance();
        if ($deviceType == 'ios'):
            $CI->db->select('reg_id_ios,id');
            $CI->db->where("reg_id_ios!=''", NULL, FALSE);
        else:
            $CI->db->select('reg_id,id');
            $CI->db->where("reg_id!=''", NULL, FALSE);
        endif;
        if ($userID != '')
            $CI->db->where('id', $userID);
        $query = $CI->db->get('users');
        
        $getuser = $query->result_array();
        return $getuser;
    }

}

if (!function_exists('getUserInfo')) {

    function getUserInfo($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

}

if (!function_exists('getResourceInfo')) {

    function getResourceInfo($data) {
        $CI = &get_instance();
        if ($data['type'] == 'post'):
            $CI->db->where('activity_id', $data['resource_id']);
            $query = $CI->db->get('activities');
            if ($query->num_rows() > 0) {
                return $query->row();
            } else {
                return false;
            }
        else:
            $CI->db->where('event_id', $data['resource_id']);
            $query = $CI->db->get('events');
            if ($query->num_rows() > 0) {
                return $query->row();
            } else {
                return false;
            }
        endif;
    }

}

if (!function_exists('insertNotification')) {

    function insertNotification($data) {
        $CI = &get_instance();
        $CI->db->insert('notifications',$data);
    }

}

if (!function_exists('androidNotification')) {

    function androidNotification($deviceIDS, $data = '') {

        $CI = &get_instance();
        $getuser = $deviceIDS;
        foreach ($getuser as $val) {
            $register_send = $val;
            $value = $register_send['reg_id'];
            $get = json_decode($value);
            $message = $data['message'];
            $title = $data['title'];
            $notiType = $data['type'];
            if ($data['type'] == 'attendance') {
                $data['data'] = array(
                    'type' => 'attendance',
                    'resource_id' => $data['event_id'],
                    'poster_id' => ''
                );
            }
            $notificationUpdateData = array(
                'resource_id' => $data['data']['resource_id'],
                'user_id' => $val['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $message,
                'device_type' => 'android',
                'status' => '0'
            );
            
            insertNotification($notificationUpdateData);
            $api_key = 'AIzaSyAUuOPKADanekXAwpLUwiJuMbzQ5PVXX0A';

            if (empty($api_key) || count($get) < 0) {
                $result = array(
                    'success' => '0',
                    'message' => 'api or reg id not found',
                );
                echo json_encode($result);
                die;
            }
            $registrationIDs = $get;
            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array("message" => $message, "title" => $title, "type" => $notiType, 'data' => $data['data']),
            );

            $headers = array(
                'Authorization: key=' . $api_key,
                'Content-Type: application/json');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);

            curl_close($ch);
            //print_r($result);
            //die;
        }
    }

}

// ios push notificatin apple mac 
if (!function_exists('iosNotification')) {

    function iosNotification($deviceIDS, $data = '') {
        $url = IMAGESPATH . 'ios-certified/apnsdev.pem';
        $CI = &get_instance();
        $getuser = $deviceIDS;
        $sound = 'default';
        $badge = '0';
        $content_aval = '1';
        $message = $data['message'];
        $title = $data['title'];
        foreach ($getuser as $valios) {
            //print_r($valios); 
            $register_send = $valios;
            $value = $register_send['reg_id_ios'];
            $gets = json_decode($value);
            if($data['type']!='attendance'){
                $notificationUpdateData = array(
               
                'user_id' => $valios['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $data['message'],
                'device_type' => 'ios',
                'status' => '0'
            );
            }else{
                $notificationUpdateData = array(
                'resource_id' => $data['event_id'],
                'user_id' => $valios['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $data['message'],
                'device_type' => 'ios',
                'status' => '0'
            );
            }
            insertNotification($notificationUpdateData);
            foreach ($gets as $get) {
                $dtoken = $get;
                $message = $message;
                // My device token here (without spaces):
                $deviceToken = $dtoken;
                // print_r($deviceToken); die;
                // My private key's passphrase here:
                $passphrase = '123456';

                // My alert message here:
                //$message = 'New Push Notification!';
                //badge
                $badge = 0;

                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', $url);
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                // Open a connection to the APNS server
                $fp = stream_socket_client(
                        'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);

                //echo 'Connected to APNS' . PHP_EOL;

                // Create the payload body
                $body['aps'] = array(
                    'alert' => $message,
                    'badge' => $badge,
                    'sound' => 'default'
                );
                $body['data'] = $data;

                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                /*if (!$result)
                    echo 'Error, notification not sent' . PHP_EOL;
                else
                    echo 'notification sent!' . PHP_EOL;*/

                // Close the connection to the server

                fclose($fp);
            }
            //print_r($result);
            //die;
        }
    }

}

if (!function_exists('iosForceNotification')) {

    function iosForceNotification($deviceIDS, $data = '') {
        
        $CI = &get_instance();
        $getuser = $deviceIDS;
        $sound = 'default';
        $badge = '0';
        $content_aval = '1';
        $message = $data['message'];
        $title = $data['title'];
        foreach ($getuser as $valios) {
            //print_r($valios); 
            $register_send = $valios;
            $value = $register_send['reg_id_ios'];
            $gets = json_decode($value);
            if($data['type']!='attendance'){
                $notificationUpdateData = array(
                'resource_id' => $data['data']['resource_id'],
                'user_id' => $valios['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $data['message'],
                'device_type' => 'ios',
                'status' => '0'
            );
            }else{
                $notificationUpdateData = array(
                'resource_id' => $data['event_id'],
                'user_id' => $valios['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $data['message'],
                'device_type' => 'ios',
                'status' => '0'
            );
            }
            insertNotification($notificationUpdateData);
        }
    }

}

if (!function_exists('PollTotalVotes')) {

    function PollTotalVotes($id) {
        $CI = &get_instance();
        $CI->db->where('poll_id',$id);
        $CI->db->select('vote_count');
        $res=$CI->db->get('polls');
        return $res->row()->vote_count;
    }

}

if (!function_exists('OptionTotalVotes')) {

    function OptionTotalVotes($id) {
        $CI = &get_instance();
        $CI->db->where('poll_option_id',$id);
        $CI->db->select('votes');
        $res=$CI->db->get('poll_options');
        return $res->row()->votes;
    }

}
?>
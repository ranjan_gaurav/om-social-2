<?php

require APPPATH . '/libraries/REST_Controller.php';

class Events extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Event_model', 'event');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function list_post() {
        $cat_id = $this->post('category_id');
        $type = $this->post('type');
        $events = $this->event->getAllEvents($cat_id, $type);
        if (!empty($events)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $events
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function event_post() {
        $id = (int) $this->post('event_id');
        $event = $this->event->getEvent($id);
        $comments = $this->event->getComments($id);
        $event[0]['comments'] = $comments;
        if (!empty($event)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $event
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function categories_post() {
        $categories = $this->event->getAllCategories();
        if (!empty($categories)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $categories
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function photos_post() {
        if (!$this->post('id') && $this->post('id') == '' && $this->post('id') == null) {
            $this->set_response([
                'status' => false,
                'response_code' => '0'
                    ], REST_Controller::HTTP_OK);
        }
        $id = (int) $this->post('id');
        $base_photo_url = base_url() . 'uploads/events/gallery/';
        $photos = $this->event->getAllEventsPhotos($id, $base_photo_url);
        if (!empty($photos)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'photos' => $photos
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function calender_post() {
        $events = $this->event->getAllCalenderEvents();
        if (!empty($events)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $events
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function attendees_post() {
        $user_id = (int) $this->post('user_id');
        $event_id = (int) $this->post('resource_id');
        $data = $this->post();
        $attendees = $this->event->insertAttendees($data);
        if ($attendees['status']) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => 'success'
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response($attendees, REST_Controller::HTTP_OK);
        }
    }

    public function listattendees_post() {
        $attendees = $this->event->listAttendees();
        if (!empty($attendees)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $attendees
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function leadership_post() {
        $attendees = $this->event->listLeadershipAttendees();
        if (!empty($attendees)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $attendees
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function notification_post() {
        $events = $this->event->GetEventsBytime();
        /* Send Notification to user */
        if (!empty($events)) {
            $notificationData = array(
                'title' => 'New Event',
                'message' => 'New Event is organized.',
                'type' => 'attendance',
                'event_id' => ($events->event_id)
            );
            $androidDeviceIDS = getUserDeviceIDs('', 'android');
            
            $iosIDS = getUserDeviceIDs('', 'ios');
            if (!empty($androidDeviceIDS)):
                androidNotification($androidDeviceIDS, $notificationData);
            endif;
            if (!empty($iosIDS)):
                iosForceNotification($iosIDS, $notificationData);
            endif;
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                    ], REST_Controller::HTTP_OK);
        }else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }

        /* Notification End */
    }

    public function makeattendance_post() {
        $data = array(
            'resource_id' => $this->post('resource_id'),
            'user_id' => $this->post('user_id'),
        );
        $insert = $this->event->insertNewAttendees($data);
        $this->event->MakeReadAttendeesNotification($data);
        $this->set_response($insert, REST_Controller::HTTP_OK);
    }

}

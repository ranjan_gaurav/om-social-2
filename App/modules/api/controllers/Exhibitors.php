<?php

require APPPATH . '/libraries/REST_Controller.php';

class Exhibitors extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Exhibitor_model', 'exhibitor');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function list_post() {
        $event_id = (int) $this->post('event_id');
        $exhibitor_id = (int) $this->post('exhibitor_id');
        $exhibitors = $this->exhibitor->getAllexhibitors($event_id,$exhibitor_id);
        if (!empty($exhibitors)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $exhibitors
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

}

<?php

require APPPATH . '/libraries/REST_Controller.php';

class Poke extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Poke_model', 'poke');
    }

    

    public function add_poke_post() {
        $data['senderuser_id'] = $this->input->post('senderuserid');
        $data['receiveruser_id'] = $this->input->post('receiveruserid');
        $type = $this->input->post('device_type');
        
      //  $getuser = $this->GetUsersData($data['receiveruser_id']);
        $create = $this->poke->addPoke($data,$type);
        $this->set_response($create, REST_Controller::HTTP_OK);
    }
    
    public function get_posts_wall_post()
    {
    	$userid = $this->input->post('user_id');
    	if($userid)
    	{
    		$GetPosts = $this->wall->GetWallPostsById($userid);
    		
    		if($GetPosts)
    		{
    			$message = [ 
						'status' => true,
						'response_code' => '1',
						'posts' => $GetPosts 
				];
    		}
    		
    		else 
    		{
    			$message = [
    					'status' => false,
    					'response_code' => '0',
    					'message' => 'No posts on your wall'
    			];
    		}
    	}
    	
    	$this->set_response ( $message, REST_Controller::HTTP_OK );
    }
    
  public function editwall_post()
    {
    	$wall_id = $this->input->post('wall_id');
    	$data['body'] = $this->input->post('body');
  	    
    	if($wall_id != '')
    	{
    	$update =  $this->wall->EditWallPost($wall_id,$data);
    	
    	}
    	
    	else {
    			
    		$update = [
    				'response_code' => '0',
    				'message' => 'Id Not Found'
    		];
    	}
    	
    	$this->set_response ( $update, REST_Controller::HTTP_OK );
    }
    
    public function deletewalls_post()
    {
    	$wall_id = $this->input->post('wall_id');
    	
    	if($wall_id != '')
    	{
    		$delete =  $this->wall->DeleteWallPost($wall_id);
    		
    		if($delete)
    		{
    			$delete = [
    					'status' => true,
    					'response_code' => '1',
    					'message' => 'Post Deleted from Wall'
    			];
    		}
    		 
    	}
    	 
    	else {
    		 
    		$delete = [
    				'response_code' => '0',
    				'message' => 'Id Not Found'
    		];
    	}
    	 
    	$this->set_response ( $delete, REST_Controller::HTTP_OK );
    }

}

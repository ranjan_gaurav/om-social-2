<?php

require APPPATH . '/libraries/REST_Controller.php';

class Wall extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Wall_model', 'wall');
    }

    public function polls_post() {
        $user_id = $this->post('user_id');
        $polls = $this->poll->getAllpolls();
        $options = $this->poll->getAllpollOptions();
        $pollList = array();
        $i = 0;
        if (!empty($options)) {
            foreach ($polls as $list) {
                $Pollid = $list['poll_id'];
                $checkPollForUser = $this->poll->checkUserPoll($list['poll_id'], $user_id);
                $pollList[$i] = $list;
                if ($checkPollForUser) {
                    $pollList[$i]['already_voted'] = '1';
                } else {
                    $pollList[$i]['already_voted'] = '0';
                }
                $pollList[$i]['options'] = $options[$Pollid];
                $i++;
            }
        }
        if (!empty($pollList)) {
            $totalUsers=$this->poll->totalUsers();
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'total_users'=>$totalUsers,
                'data' => $pollList
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function add_post_wall_post() {
        $data['senderuser_id'] = $this->input->post('senderuserid');
        $data['receiveruser_id'] = $this->input->post('receiveruserid');
        $data['body'] = $this->input->post('body');
        $sendername = $this->input->post('senderusername');
      //  $getuser = $this->GetUsersData($data['receiveruser_id']);
        $create = $this->wall->addWallPost($data,$sendername);
        $this->set_response($create, REST_Controller::HTTP_OK);
    }
    
    public function get_posts_wall_post()
    {
    	$userid = $this->input->post('user_id');
    	$walluserid = $this->input->post('wall_user_id');
    	//$page = $this->input->post('page');
    	
    	if($userid)
    	{
    		/* if (isset($page))
    		{
    			$pageno = $this->input->post('page');
    		}
    		
    		else 
    		{
    			$pageno = '1';
    		}
    		$start_from = ($pageno-1) * 4; */
    		$GetPosts = $this->wall->GetWallPostsById($userid,$walluserid);
    		
    		if($GetPosts)
    		{
    			$message = [ 
						'status' => true,
						'response_code' => '1',
						'posts' => $GetPosts 
				];
    		}
    		
    		else 
    		{
    			$message = [
    					'status' => false,
    					'response_code' => '0',
    					'message' => 'No posts on your wall'
    			];
    		}
    	}
    	
    	$this->set_response ( $message, REST_Controller::HTTP_OK );
    }
    
  public function editwall_post()
    {
    	$wall_id = $this->input->post('wall_id');
    	$data['body'] = $this->input->post('body');
  	    
    	if($wall_id != '')
    	{
    	$update =  $this->wall->EditWallPost($wall_id,$data);
    	
    	}
    	
    	else {
    			
    		$update = [
    				'response_code' => '0',
    				'message' => 'Id Not Found'
    		];
    	}
    	
    	$this->set_response ( $update, REST_Controller::HTTP_OK );
    }
    
    public function deletewalls_post()
    {
    	$wall_id = $this->input->post('wall_id');
    	
    	if($wall_id != '')
    	{
    		$delete =  $this->wall->DeleteWallPost($wall_id);
    		
    		if($delete)
    		{
    			$delete = [
    					'status' => true,
    					'response_code' => '1',
    					'message' => 'Post Deleted from Wall'
    			];
    		}
    		 
    	}
    	 
    	else {
    		 
    		$delete = [
    				'response_code' => '0',
    				'message' => 'Id Not Found'
    		];
    	}
    	 
    	$this->set_response ( $delete, REST_Controller::HTTP_OK );
    }

}

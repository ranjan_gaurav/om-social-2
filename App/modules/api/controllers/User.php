<?php

/**
 * Created by PhpStorm.
 * User: Gaurav <ranjan.gaurav@orangemantra.in>
 * Date: 03/22/2017
 * Time: 1:00 PM
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class User extends REST_Controller {
	function __construct() {
		
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'user_model' );
		$this->load->helper ( 'string' );
	}
	
	/*
	 * Author : Gaurav Ranjan
	 *
	 * Date : 22-03-2017
	 *
	 * Description : Api for user-authentication
	 */
	function checkUserAuthentication_post() {
		
		// echo 56456; die();
		$user_id = $this->post ( 'user_id', true );
		$password = $this->post ( 'user_password', true );
		$new_reg_id = $this->input->post ( 'reg_id', true );
		$deviceType = $this->input->post ( 'device_type', true );
		$fcm = $this->input->post ( 'fcm_token_id', true );
		
		$checkUser = $this->user_model->validateUserdata ( $user_id, md5 ( $password ) );
		
		if ($checkUser) {
			
			if ($deviceType == 'ios') {
				$checkios = $this->user_model->checkreg_id_ios ( 'users', $user_id );
				$reg_id_ios = json_decode ( $checkios->reg_id_ios );
				if (! empty ( $reg_id_ios )) {
					if (! in_array ( $new_reg_id, $reg_id_ios )) {
						$reg_id_ios [] = $new_reg_id;
						$this->user_model->updateregid_ios ( 'users', $user_id, $reg_id_ios, $deviceType );
					}
				} else {
					$reg_id_ios [] = $new_reg_id;
					$this->user_model->updateregid_ios ( 'users', $user_id, $reg_id_ios, $deviceType );
				}
			} else {
				$check = $this->user_model->checkreg_id ( 'users', $user_id );
				
				$reg_id = json_decode ( $check->reg_id );
				if (! empty ( $reg_id )) {
					if (! in_array ( $new_reg_id, $reg_id )) {
						$reg_id [] = $new_reg_id;
						$this->user_model->updateregid ( 'users', $user_id, $reg_id, $deviceType );
					}
				} else {
					$reg_id [] = $new_reg_id;
					$this->user_model->updateregid ( 'users', $user_id, $reg_id, $deviceType );
				}
			}
			$update_fcm = $this->user_model->UpdateFCM ( 'users', $user_id, $fcm );
			$user_id = $checkUser->id;
			$user_name = $checkUser->user_name;
			$user_pic = ($checkUser->user_pic != '' && $checkUser->user_pic != null) ? base_url () . 'uploads/users/profile/' . $checkUser->user_pic : "";
			if ($checkUser->status == 0) {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'Not validated!' 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$Res = $checkUser;
				$Res->user_pic = $user_pic;
				$removeKeys = array (
						'user_pass',
						'register_type',
						'activation_key',
						'registered_date',
						'user_modification_date',
						'random_code',
						'status' 
				);
				
				foreach ( $removeKeys as $key ) {
					unset ( $Res->$key );
				}
				$this->set_response ( [ 
						'status' => true,
						'response_code' => '1',
						'message' => 'Login Successfull',
						'data' => $checkUser 
				], REST_Controller::HTTP_OK );
				
				// $this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$this->set_response ( [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Invalid Credentials' 
			], REST_Controller::HTTP_OK );
		}
	}
	
	/*
	 * Author : Gaurav Ranjan
	 *
	 * Date : 22-03-2017
	 *
	 * Description : Api for New User Registration
	 */
	public function registerUser_post() {
		error_reporting ( 0 );
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		$posted_data = $this->post ();
		$randcode = random_string ( 'numeric', 16 );
		$random_code = substr ( $randcode, 0, 4 );
		$data = array (
				'user_id' => $this->post ( 'user_id' ),
				'user_email' => $this->post ( 'user_email' ),
				'user_password' => md5 ( $this->post ( 'user_password' ) ),
				'user_address' => $this->post ( 'user_address' ) ? $this->post ( 'user_address' ) : '',
				'user_name' => $this->post ( 'user_name' ) ? $this->post ( 'user_name' ) : '',
				'user_nicename' => $this->post ( 'user_name' ) ? $this->slugify ( $this->post ( 'user_name' ) ) : '',
				'user_mobile' => $this->post ( 'user_mobile' ),
				'user_dob' => $this->post ( 'user_dob' ) ? $this->post ( 'user_dob' ) : '',
				'user_gender' => $this->post ( 'user_gender' ) ? $this->post ( 'user_gender' ) : '',
				'random_code' => $random_code,
				'status' => '1' 
		);
		
		$check = $this->user_model->checkUser ( $posted_data );
		if ($check) {
			$create = array (
					'status' => false,
					'response_code' => '0',
					'message' => 'Already registerd.Please Continue!' 
			);
		} else {
			$create = $this->user_model->create ( $data );
		}
		
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	public function slugify($text) {
		$text = preg_replace ( '~[^\\pL\d]+~u', '_', $text );
		$text = trim ( $text, '-' );
		$text = iconv ( 'utf-8', 'us-ascii//TRANSLIT', $text );
		$text = strtolower ( $text );
		$text = preg_replace ( '~[^-\w]+~', '', $text );
		if (empty ( $text )) {
			return 'n-a';
		}
		return $text;
	}
	
	/**
	 * Author: Gaurav Ranjan
	 *
	 * Date : 22/03/2017
	 *
	 * Description : Update User data
	 */
	public function updateUser_post() {
		$id = $this->post ( 'id' );
		if (isset ( $id ) && $id != '') {
			$update = $this->user_model->update ( $id, $this->post () );
			if ($update ['status']) {
				$status = REST_Controller::HTTP_OK;
			} else {
				$status = REST_Controller::HTTP_OK;
			}
			$this->set_response ( $update, $status );
		} else {
			
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Please Enter User_id' 
			];
			
			$this->response ( $message, REST_Controller::HTTP_BAD_REQUEST ); // BAD_REQUEST (400) being the HTTP response code
		}
	}
	
	/**
	 * Author: Gaurav Ranjan
	 *
	 * Date : 22/03/2017
	 *
	 * Description : delete User data
	 */
	public function dropUser_post() {
		
		// echo 989; die();
		$id = $this->input->post ( 'id' );
		
		// Validate the id.
		if (isset ( $id ) && $id == '') {
			// Set the response and exit
			
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Please Enter User_id' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} 

		else {
			$result = $this->user_model->delete ( $id );
			
			if ($result) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'message' => 'User Successfully Deleted' 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'Bad request' 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		}
	}
	
	
	/*
	 * Author : Gaurav Ranjan
	 * 
	 * Date: 22-03-2017
	 * 
	 * Description : Forgot password api
	 */
	public function forgotpassword_post() {
		
		error_reporting(0);
		
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		
		$data = array (
				'user_email' => $this->post ( 'user_email' ) 
		);
		$checkCredentails = $this->user_model->CheckUserCredentials ( $data );
		if ($checkCredentails) {
			$create = $this->user_model->validate_password ( $data );
			if ($create ['status']) {
				$status = REST_Controller::HTTP_OK;
				$this->set_response ( $create, $status );
			} else {
				
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => $create ['message'] 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Sorry! Invalid Credentails.Email Not found!' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	public function userList_get() {
		$id = ( int ) $this->get ( 'id' );
		log_message ( 'info', 'data=' . $id );
		$users = $this->user_model->getAllUsers ( $id );
	
		if (! empty ( $users )) {
			$this->set_response ( [
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'data' => $users
			], REST_Controller::HTTP_OK ); // NOT_FOUND (404) being the HTTP response code
		} else {
			$this->set_response ( [
					'status' => FALSE,
					'response_code' => '0',
					'message' => 'User could not be found'
			], REST_Controller::HTTP_OK ); // NOT_FOUND (404) being the HTTP response code
		}
	}
	
	
	/* // change password
	function changepassword_post() {
		error_reporting ( 0 );
		
		$user_id = $this->post ( 'userid', true );
		$current_pass = $this->post ( 'current_pass', true );
		$new_pass = $this->post ( 'new_pass', true );
		if ($user_id != '') {
			$userpasswrod = $this->user_model->valid_current_pass ( $user_id, md5 ( $current_pass ) );
			if (! $userpasswrod) {
				$message = [ 
						'response_code' => '0',
						'message' => 'Invalid Current Password' 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				if ($this->post ( 'is_reset', true )) {
					$reset = '1';
				} else {
					$reset = '';
				}
				$this->user_model->updatepassword ( 'users', $user_id, $new_pass, $reset );
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'new_password' => $new_pass 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Invalid Credentials' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
		
		return;
		
		$data ['main_content'] = 'signin';
		$this->load->view ( 'full_width', $data );
	}
	protected function changeprofilepic_post() {
		$user_id = ( int ) $this->post ( 'user_id' );
		if (isset ( $user_id ) && $user_id != '' && $user_id != null) {
			$update = $this->user_model->updateprofile ( $user_id );
			if ($update) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'user_pic' => $update 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Not Updated' 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Invalid User' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	// logout
	public function logout_post() {
		$user_id = $this->post ( 'userid', true );
		// $reg_id = $this->input->post('reg_id', true);
		$type = $this->input->post ( 'type', true );
		$fcm = $this->input->post ( 'fcm_token_id', true );
		
		if ($user_id != '') {
			
			if ($type == 'ios') {
				
				$data ['reg_id_ios'] = '';
				$data ['fcm_token_id'] = '';
				$create = $this->user_model->updaterlogout_ios ( 'users', $user_id, $data, $fcm );
				$message = [ 
						'response_code' => '1',
						'userid' => $user_id,
						'message' => 'Successfully Logout' 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				
				$data ['reg_id'] = '';
				$data ['fcm_token_id'] = '';
				$create = $this->user_model->updaterlogout ( 'users', $user_id, $data, $fcm );
				$message = [ 
						'response_code' => '1',
						'userid' => $user_id,
						'message' => 'Successfully Logout' 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Sorry! Invalid Userid. and reg id' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function invite_post() {
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		$posted_data = $this->post ();
		
		$data = array (
				'userid' => $this->post ( 'userid' ),
				'invite_name' => $this->post ( 'name' ),
				'invite_email' => $this->post ( 'email_id' ),
				'status' => '1' 
		);
		
		if ($data ['userid'] != '') {
			
			$create = $this->user_model->createinvite ( $data );
			$this->set_response ( $create, REST_Controller::HTTP_OK );
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Sorry! Invalid Userid.' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	} */
}

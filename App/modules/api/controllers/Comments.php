<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 04/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Comments extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Comments_model', 'comment');
        $this->load->model('Event_model', 'event');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function create_post() {

        $user_id = (int) $this->post('poster_id');
        $event_id = (int) $this->post('resource_id');
        $event_type = $this->post('type');
        $comment_body = $this->post('body');
        $data = $this->post();
        $comments = $this->comment->insertComments($data);
        if ($comments['status']) {

            /* Send Notification to user */
            $ResourceDetails = getResourceInfo($data);

            $resUserInfo = getUserInfo($ResourceDetails->user_id);

            $notificationData = array(
                'title' => 'New Comment',
                'message' => 'You have new Comment for your post.',
                'type' => 'comment',
                'data' => $data,
            );
            
            $DeviceIDS = getUserDeviceIDs($ResourceDetails->user_id, 'android');
            $iosIDS = getUserDeviceIDs($ResourceDetails->user_id, 'ios');
            if (!empty($DeviceIDS)):
                androidNotification($DeviceIDS, $notificationData);
            endif;
            if (!empty($iosIDS)):
                iosForceNotification($iosIDS, $notificationData);
            endif;
            /* Notification End */

            if ($event_type == 'event') {
                $event_comment_counter = $this->event->getCountComment($event_id);
                $increment_counter = $event_comment_counter->comment_count + 1;
            }if ($event_type == 'post') {
                $event_activity_counter = $this->event->getCountActivity($event_id);
                $increment_counter = $event_activity_counter->comment_count + 1;
            }
            $this->event->updateCounter($increment_counter, $event_id, $event_type);
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'comments' => 'success',
                'comment_id' => $comments['comment_id'],
                'data' => 'success'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response($comments, REST_Controller::HTTP_OK);
        }
    }

    public function list_post() {
        $list_type = $this->uri->segment(3);
        if (!$this->post('resource_id') && $this->post('resource_id') == '' && $this->post('resource_id') == null) {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
        $event_id = $this->post('resource_id');
        $comments = $this->comment->getAllComments($event_id, $list_type);
        if (!empty($comments)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $comments
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function drop_post() {
        $id = (int) $this->post('comment_id');
        if ($id <= '0') {
            $message = [
                'response_code' => '0',
                'message' => 'No Comment ID'
            ];
            $this->response($message, REST_Controller::HTTP_OK);    // BAD_REQUEST (400) being the HTTP response code
        }
        $resource_id = $this->comment->getActivity($id);
        $this->comment->updateComment($resource_id->resource_id);
        $result = $this->comment->delete($id);
        if ($result) {
            $message = [
                'response_code' => '1',
                'message' => 'Comment Deleted'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Bad request'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

}

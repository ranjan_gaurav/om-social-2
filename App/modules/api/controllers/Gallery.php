<?php

require APPPATH . '/libraries/REST_Controller.php';

class Gallery extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Gallery_model', 'gallery');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function photos_post() {
        $photos = $this->gallery->getAllGalleryPhotos();
        if (!empty($photos)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'photos' => $photos
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function addgallery_post() {
        if ($this->post('user_id') != '') {
            $file = $this->gallery->UploadGalleryPhoto();
            $data = array(
                'event_id' => '0',
                'user_id' => $this->post('user_id'),
                'title' => '',
                'description' => '',
                'status' => '1'
            );
            if ($file['file_name'] != '') {
                $data['photo_name'] = $file['file_name'];
            }
            $insert = $this->gallery->AddGalleryPhoto($data);
            if ($insert) {
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => 'Photo Uploaded Successfully !'
                );
            } else {
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'Not Uploaded.File is too large. !'
                );
            }
        } else {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => 'Invalid user id !'
            );
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

}

<?php

/**
 *
 * @author Gaurav Ranjan  <ranjan.gaurav@orangemantra.in>
 * This is used for the api purpose
 *
 */
require APPPATH . '/libraries/REST_Controller.php';
class Groups extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		error_reporting ( 0 );
		$this->load->model ( 'Groups_model', 'groups' );
	}
	public function polls_post() {
		$user_id = $this->post ( 'user_id' );
		$polls = $this->poll->getAllpolls ();
		$options = $this->poll->getAllpollOptions ();
		$pollList = array ();
		$i = 0;
		if (! empty ( $options )) {
			foreach ( $polls as $list ) {
				$Pollid = $list ['poll_id'];
				$checkPollForUser = $this->poll->checkUserPoll ( $list ['poll_id'], $user_id );
				$pollList [$i] = $list;
				if ($checkPollForUser) {
					$pollList [$i] ['already_voted'] = '1';
				} else {
					$pollList [$i] ['already_voted'] = '0';
				}
				$pollList [$i] ['options'] = $options [$Pollid];
				$i ++;
			}
		}
		if (! empty ( $pollList )) {
			$totalUsers = $this->poll->totalUsers ();
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'total_users' => $totalUsers,
					'data' => $pollList 
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'No Content' 
			], REST_Controller::HTTP_OK );
		}
	}
	public function addgroups_post() {
		error_reporting ( 0 );
		// $photo_name = $this->input->post ( 'photo_name' );
		$data = $this->post ();
		unset ( $data ['users_to_add'] );
		// unset ( $data ['photo_name'] );
		$data ['status'] = 1;
		$userid = $this->input->post ( 'group_admin' );
		
		$Sendername = $this->groups->GetUserName ( $userid );
		
		$file = $this->groups->UploadGalleryPhoto ();
		if ($file ['file_name'] != '') {
			$data ['photo_name'] = $file ['file_name'];
		}
		
		$users = $this->input->post ( 'users_to_add' );
		$json = json_decode ( $users );
		array_push ( $json, $userid );
		
		$create = $this->groups->CreateGroups ( $data, $json, $Sendername, $userid );
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	public function deletegroup_post() {
		$id = $this->input->post ( 'groupid' );
		$userid = $this->input->post ( 'userid' );
		if ($id != '') {
			
			$groupadmin = $this->groups->GetGroupAdmin ( $id );
			
			if ($groupadmin == $userid || $userid == 1) {
				
				$delete = $this->groups->delete_group ( $id );
				if ($delete) {
					$message = [ 
							'status' => true,
							'response_code' => '1',
							'message' => 'Group Deleted' 
					];
				}
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => "You Don't have the permission to delete this group" 
				];
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Please Add Group ID' 
			], REST_Controller::HTTP_OK );
		}
	}
	public function add_group_members_post() {
		$groupid = $this->input->post ( 'groupid' );
		$users = $this->input->post ( 'users_to_add' );
		$json = json_decode ( $users );
		// $arr = explode ( ',', $users );
		$arr = implode ( ",", $json );
		
		// print_r($arr); exit;
		
		$check = $this->groups->checkmembers ( $groupid, $arr );
		
		if ($check) {
			$message = $this->groups->add_Group_users ( $json, $groupid );
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'users already added in this group' 
			];
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function remove_group_member_post() {
		$groupid = $this->input->post ( 'groupid' );
		$userid = $this->input->post ( 'userid' );
		
		if ($groupid != '') {
			
			$admin = $this->groups->GetGroupAdmin ( $groupid );
			
			// print_r($admin); die();
			
			$delete = $this->groups->RemoveGroupMember ( $groupid, $userid );
			
			if ($delete) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'message' => 'Member Deleted' 
				];
				
				if ($userid == $admin) {
					$Getusers = $this->groups->SeeGroupMembers ( $groupid );
					
					$listofusers = '';
					foreach ( $Getusers as $list ) {
						
						$listofusers [] = $list->id;
					}
					
					$key = array_rand ( $listofusers );
					$value ['group_admin'] = $listofusers [$key];
					
					$update = $this->groups->updateGroupAdmin ( $groupid, $value );
				}
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function add_groupPost_post() {
		$data ['post_title'] = $this->input->post ( 'post_title' );
		$data ['post_description'] = $this->input->post ( 'post_description' );
		$data ['user_id_fk'] = $this->input->post ( 'user_id' );
		$data ['group_id_fk'] = $this->input->post ( 'group_id' );
		
		$userid = $this->input->post ( 'user_id' );
		$groupid = $this->input->post ( 'group_id' );
		
		$check = $this->groups->checkGroupmembers ( $groupid, $userid );
		
		if ($check == 1) {
			
			$getusers = $this->groups->GetGroupUsers ( $groupid );
			
			$userslist = array ();
			foreach ( $getusers as $list ) {
				$val = $list->user_id_fk;
				array_push ( $userslist, $val );
			}
			
			$create = $this->groups->AddGroupPost ( $data, $groupid, $userslist, $userid );
		} 

		else {
			$create = array (
					'status' => False,
					'response_code' => '0',
					'message' => "Sorry, You are not a member of this group" 
			);
		}
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	public function edit_groupPost_post() {
		$Postid = $this->input->post ( 'post_id' );
		$data = $this->input->post ();
		unset ( $data ['post_id'] );
		
		if (isset ( $Postid ) && $Postid != '') {
			
			$update = $this->groups->UpdatePost ( $Postid, $data );
		} 

		else {
			
			$update = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $update, REST_Controller::HTTP_OK );
	}
	public function delete_groupPost_post() {
		$groupid = $this->input->post ( 'groupid' );
		$postid = $this->input->post ( 'postid' );
		
		if ($groupid != '') {
			
			$check = $this->groups->checkPost ( $postid );
			if ($check) {
				
				$delete = $this->groups->RemoveGroupPost ( $groupid, $postid );
				
				if ($delete) {
					$message = [ 
							'status' => true,
							'response_code' => '1',
							'message' => 'Post Deleted' 
					];
				}
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'Post not found' 
				];
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getAlluser_post() {
		$userId = $this->input->post ( 'userid' );
		
		if ($userId != '') {
			$Getusers = $this->groups->GetAllUsers ( $userId );
			
			if ($Getusers) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'users' => $Getusers 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getAllgroups_post() {
		$GetGroups = $this->groups->GetAllGroups ();
		
		if ($GetGroups) {
			$message = [ 
					'status' => true,
					'response_code' => '1',
					'users' => $GetGroups 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getgroupMembers_post() {
		$groupid = $this->input->post ( 'groupid' );
		
		if ($groupid != '') {
			$members = $this->groups->SeeGroupMembers ( $groupid );
			
			if (count ( $members ) > 0) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'group_id' => $groupid,
						'users' => $members 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Members Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getAllgroupPosts_post() {
		$comments = array ();
		$groupid = $this->input->post ( 'groupid' );
		$userid = $this->input->post ( 'userid' );
		$page = $this->input->post('page');
		if ($groupid != '') {
			
			 if (isset($page))
			{
				$pageno = $this->input->post('page');
			} 
			
			 else
			{
				$pageno = '1';
			}
			$start_from = ($pageno-1) * 10; 
			
			$posts = $this->groups->SeeGroupPosts ( $groupid, $userid, $start_from);
			// $comm = $this->groups->getAllComments ( $groupid );
			
			if (count ( $posts ) > 0) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'group_id' => $groupid,
						'data' => $posts 
				];
				// 'comments'=>$comm
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Post Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function likepost_post() {
		$data ['user_id'] = $this->input->post ( 'userid' );
		$data ['poster_id'] = $this->input->post ( 'postid' );
		$data ['poster_type'] = $this->input->post ( 'poster_type' );
		$data ['type'] = $this->input->post ( 'activity_type' );
		
		$UserName = $this->input->post ( 'username' );
		$Type = $this->input->post ( 'activity_type' );
		
		$Likes = $this->groups->AddLikes ( $data, $UserName, $Type, $data ['user_id'] );
		$this->set_response ( $Likes, REST_Controller::HTTP_OK );
	}
	public function AddComments_post() {
		$data ['group_id'] = $this->input->post ( 'groupid' );
		$data ['user_id'] = $this->input->post ( 'userid' );
		$data ['poster_id'] = $this->input->post ( 'postid' );
		$data ['poster_type'] = $this->input->post ( 'poster_type' );
		$data ['body'] = $this->input->post ( 'body' );
		$data ['type'] = $this->input->post ( 'activity_type' );
		
		$UserName = $this->input->post ( 'username' );
		$Type = $this->input->post ( 'activity_type' );
		
		$Comments = $this->groups->AddComments ( $data, $UserName, $Type, $data ['user_id'] );
		$this->set_response ( $Comments, REST_Controller::HTTP_OK );
	}
	public function getcommentsByPostID_post() {
		$postid = $this->input->post ( 'post_id' );
		$type = $this->input->post ( 'activity_type' );
		$GetComments = $this->groups->GetCommentsByPostID ( $postid, $type );
		
		if ($GetComments) {
			$message = [ 
					'status' => true,
					'response_code' => '1',
					'comments' => $GetComments 
			];
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'No Comments Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function deletecommentByid_post() {
		$commentID = $this->input->post ( 'comment_id' );
		$postID = $this->input->post ( 'post_id' );
		
		if (isset ( $commentID ) && $commentID != '') {
			
			$delete = $this->groups->RemoveComments ( $commentID, $postID );
			
			if ($delete) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'message' => 'Comment Deleted' 
				];
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getgroupByuserid_post() {
		$userid = $this->input->post ( 'userid' );
		if ($userid != '') {
			$GetGroups = $this->groups->GetGroupsByID ( $userid );
			
			if ($GetGroups) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'Groups' => $GetGroups 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No Groups Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function editgroups_post() {
		$groupid = $this->input->post ( 'group_id' );
		$data = $this->input->post ();
		unset ( $data ['group_id'] );
		
		$file = $this->groups->UploadGalleryPhoto ();
		if ($file ['file_name'] != '') {
			$data ['photo_name'] = $file ['file_name'];
		}
		
		if (isset ( $groupid ) && $groupid != '') {
			
			$update = $this->groups->UpdateGroups ( $groupid, $data );
		} 

		else {
			
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $update, REST_Controller::HTTP_OK );
	}
}

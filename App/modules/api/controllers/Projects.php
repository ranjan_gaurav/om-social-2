<?php
require APPPATH . '/libraries/REST_Controller.php';
class Projects extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'Project_model', 'projects' );
	}
	public function addprojects_post() {
		// error_reporting(0);
		// $photo_name = $this->input->post ( 'photo_name' );
		$data = $this->post ();
		unset ( $data ['users_to_add'] );
		unset ($data['username']);
		// unset ( $data ['photo_name'] );
		$userid = $this->input->post ( 'project_admin' );
		$Sendername = $this->input->post('username');
		$file = $this->projects->UploadGalleryPhoto ();
		if ($file ['file_name'] != '') {
			$data ['photo_name'] = $file ['file_name'];
		}
		
		$users = $this->input->post ( 'users_to_add' );
		$json = json_decode ( $users );
		array_push ( $json, $userid );
		
		$create = $this->projects->CreateProjects ( $data, $json ,$userid,$Sendername);
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	public function edit_project_post() {
		$projectid = $this->input->post ( 'project_id' );
		$data = $this->input->post ();
		unset ( $data ['project_id'] );
		
		$file = $this->projects->UploadGalleryPhoto ();
		if ($file ['file_name'] != '') {
			$data ['photo_name'] = $file ['file_name'];
		}
		
		if (isset ( $projectid ) && $projectid != '') {
			
			$update = $this->projects->UpdateProjects ( $projectid, $data );
		} 

		else {
			
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $update, REST_Controller::HTTP_OK );
	}
	public function delete_project_post() {
		$ProjectID = $this->input->post ( 'project_id' );
		$userid = $this->input->post ( 'user_id' );
		
		if ($ProjectID != '') {
			
			$projectadmin = $this->projects->GetProjectAdmin ( $ProjectID );
			
			if ($projectadmin == $userid || $userid == 1) {
				
				$delete = $this->projects->RemoveProject ( $ProjectID );
				
				if ($delete) {
					$message = [ 
							'status' => true,
							'response_code' => '1',
							'message' => 'Project Deleted' 
					];
				}
			} 

			else {
				
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => "You Don't have the permission to delete this project" 
				];
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getAllprojects_post() {
		$GetProjects = $this->projects->GetAllProjects ();
		
		if ($GetProjects) {
			$message = [ 
					'status' => true,
					'response_code' => '1',
					'projects' => $GetProjects 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function GetProjectsByUserid_post() {
		$UserID = $this->input->post ( 'user_id' );
		$GetProjects = $this->projects->GetProjectsByID ( $UserID );
		
		if (count ( $GetProjects ) > 0) {
			$message = [ 
					'status' => true,
					'response_code' => '1',
					'Groups' => $GetProjects 
			];
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'No projects found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function add_project_members_post() {
		$projectid = $this->input->post ( 'project_id' );
		$users = $this->input->post ( 'users_to_add' );
		$json = json_decode ( $users );
		// $arr = explode ( ',', $users );
		$arr = implode ( ",", $json );
		
		// print_r($arr); exit;
		
		$check = $this->projects->checkmembers ( $projectid, $arr );
		
		if ($check) {
			$message = $this->projects->add_Project_users ( $json, $projectid );
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'users already added in this project' 
			];
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function remove_project_member_post() {
		$projectid = $this->input->post ( 'project_id' );
		$userid = $this->input->post ( 'user_id' );
		
		if ($projectid != '') {
			
			$admin = $this->projects->GetProjectAdmin($projectid);
			
			$delete = $this->projects->RemoveProjectMember ( $projectid, $userid );
			
			if ($delete) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'message' => 'Member Deleted' 
				];
				
				if($userid == $admin)
				{
					$Getusers = $this->projects->SeeProjectMembers ( $projectid );
						
					$listofusers = '';
					foreach($Getusers as $list)
					{
				
						$listofusers[] = $list->id;
					}
						
					$key = array_rand($listofusers);
					$value['project_admin'] = $listofusers[$key];
					 
					$update = $this->projects->updateProjectAdmin($projectid,$value);
				}
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getprojectMembers_post() {
		$projectid = $this->input->post ( 'project_id' );
		
		if ($projectid != '') {
			$members = $this->projects->SeeProjectMembers ( $projectid );
			
			if (count ( $members ) > 0) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'porject_id' => $projectid,
						'users' => $members 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Members Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function add_projectPost_post() {
		$data ['post_title'] = $this->input->post ( 'post_title' );
		//$data ['post_description'] = $this->input->post ( 'post_description' );
		$data ['user_id_fk'] = $this->input->post ( 'user_id' );
		$data ['project_id_fk'] = $this->input->post ( 'project_id' );
		
		$userid = $this->input->post ( 'user_id' );
		$projectid = $this->input->post ( 'project_id' );
		
		$check = $this->projects->checkProjectmembers ( $projectid, $userid );
		
		if ($check == 1) {
			$create = $this->projects->AddProjectPost ( $data );
		} 

		else {
			$create = array (
					'status' => False,
					'response_code' => '0',
					'message' => "Sorry, You are not a member of this project" 
			);
		}
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	public function edit_projectPost_post() {
		$Postid = $this->input->post ( 'post_id' );
		$data = $this->input->post ();
		unset ( $data ['post_id'] );
		
		if (isset ( $Postid ) && $Postid != '') {
			
			$update = $this->projects->UpdateProjectPost ( $Postid, $data );
		} 

		else {
			
			$update = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $update, REST_Controller::HTTP_OK );
	}
	public function delete_projectPost_post() {
		$projectid = $this->input->post ( 'project_id' );
		$postid = $this->input->post ( 'post_id' );
		
		if ($projectid != '') {
			
			$check = $this->projects->checkPost ( $postid );
			if ($check) {
				
				$delete = $this->projects->RemoveProjectPost ( $projectid, $postid );
				
				if ($delete) {
					$message = [ 
							'status' => true,
							'response_code' => '1',
							'message' => 'Post Deleted' 
					];
				}
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'Post not found' 
				];
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getAllProjectPosts_post() {
		$comments = array ();
		$projectid = $this->input->post ( 'project_id' );
		$userid = $this->input->post ( 'user_id' );
		
		if ($projectid != '') {
			$posts = $this->projects->SeeProjectPosts ( $projectid, $userid );
			// $comm = $this->groups->getAllComments ( $groupid );
			
			if (count ( $posts ) > 0) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'project_id' => $projectid,
						'data' => $posts 
				];
				// 'comments'=>$comm
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Post Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
}

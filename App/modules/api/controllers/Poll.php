<?php

require APPPATH . '/libraries/REST_Controller.php';

class Poll extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Poll_model', 'poll');
    }

    public function polls_post() {
        $user_id = $this->post('user_id');
        $polls = $this->poll->getAllpolls();
        $options = $this->poll->getAllpollOptions();
        $pollList = array();
        $i = 0;
        if (!empty($options)) {
            foreach ($polls as $list) {
                $Pollid = $list['poll_id'];
                $checkPollForUser = $this->poll->checkUserPoll($list['poll_id'], $user_id);
                $pollList[$i] = $list;
                if ($checkPollForUser) {
                    $pollList[$i]['already_voted'] = '1';
                } else {
                    $pollList[$i]['already_voted'] = '0';
                }
                $pollList[$i]['options'] = $options[$Pollid];
                $i++;
            }
        }
        if (!empty($pollList)) {
            $totalUsers=$this->poll->totalUsers();
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'total_users'=>$totalUsers,
                'data' => $pollList
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function addpoll_post() {
        $data = $this->post();
        $create = $this->poll->setpollVote($data);
        $this->set_response($create, REST_Controller::HTTP_OK);
    }

}

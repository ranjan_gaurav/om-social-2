<?php

require APPPATH . '/libraries/REST_Controller.php';

class Activity extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('activity_model', 'activity');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function list_post() {
        $comments = $this->activity->getComments();
        $id = (int) $this->post('id');
        $user_id = (int) $this->post('user_id');
        $page = $this->input->post('page');
         if (isset($page))
        {
        	$pageno = $this->input->post('page');
        }
        	
        else
        {
        	$pageno = '1';
        }
        $start_from = ($pageno-1) * 10; 
        	
        
        $TopActivity = $this->activity->getTopActivity($user_id);
        $allActivities = $this->activity->getAllActivities($id, $user_id,$start_from);
        if($page == 1)
        {
        $activities = array_merge($TopActivity,$allActivities);
        }
        else 
        {
        	$activities = $allActivities;
        }
        
        
      //  print_r($activities);
       // exit;
        
        if (!empty($activities)) {
            $resultArray = array();
            $i = 0;
            foreach ($activities as $actList) {
                $resultArray[$i] = $actList;
                $actID = $actList['activity_id'];
                if (!empty($comments[$actID])):
                    $resultArray[$i]['comments'] = $comments[$actID];
                else:
                    $resultArray[$i]['comments'] = (object) array();
                endif;
                $i++;
            }
            
           // print_r($resultArray); die();
            
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'data' => $resultArray
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function add_post() {
        $data = array(
            'user_id' => $this->post('user_id'),
            'body' => $this->post('body'),
        );
        $create = $this->activity->addActivity($data);
        $this->set_response($create, REST_Controller::HTTP_OK);
    }

    public function drop_post() {
        $id = (int) $this->post('activity_id');
        if ($id <= '0') {
            $message = [
                'response_code' => '0',
                'message' => 'No Activity ID'
            ];
            $this->response($message, REST_Controller::HTTP_OK);    // BAD_REQUEST (400) being the HTTP response code
        }
        $result = $this->activity->delete($id);
        if ($result) {
            $message = [
                'response_code' => '1',
                'message' => 'Activity Deleted'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Bad request'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function likes_post() {
        $data = array(
            'type' => $this->post('type'),
            'resource_id' => $this->post('resource_id'),
            'poster_id' => $this->post('poster_id'),
            'device_type' => $this->post('device_type') ? $this->post('device_type') : ''
        );
        $addLike = $this->activity->addLike($data);
        if ($addLike['status'] == true) {
            /* Send Notification to user */
            $ResourceDetails = getResourceInfo($data);

            $resUserInfo = getUserInfo($ResourceDetails->user_id);

            $notificationData = array(
                'title' => 'New Like',
                'message' => 'You have new like for your post.',
                'type' => 'like',
                'data' => $data,
            );
            $DeviceIDS = getUserDeviceIDs($ResourceDetails->user_id, 'android');
            $iosIDS = getUserDeviceIDs($ResourceDetails->user_id, 'ios');
            if (!empty($DeviceIDS)):
                androidNotification($DeviceIDS, $notificationData);
            endif;
            if (!empty($iosIDS)):
                iosForceNotification($iosIDS, $notificationData);
            endif;
            /* Notification End */

            $incrementPost = $this->activity->incrementLike($data);
            if ($incrementPost) {
                $addLike['total'] = $incrementPost;
            }
            $this->set_response($addLike, REST_Controller::HTTP_OK);
        } else {
            $this->set_response($addLike, REST_Controller::HTTP_OK);
        }
    }

}

<?php
class Groups_model extends CI_Model {
	var $table = 'groups';
	var $table_group = 'group_users';
	var $table_post = 'group_post';
	var $user_table = 'users';
	var $Likes = 'activity_likes';
	var $Comments = 'activity_comments';
	var $act = 'activities';
	var $Home_comments = 'event_comments';
	var $wall = 'users_wall';
	function __construct() {
		parent::__construct ();
		date_default_timezone_set ( 'Asia/Kolkata' );
	}
	public function CreateGroups($data, $arr, $Sendername, $userid) {
		error_reporting ( 0 );
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'group_name',
							'label' => 'group_name',
							'rules' => 'trim|required|is_unique[groups.group_name]' 
					),
					array (
							'field' => 'group_admin',
							'label' => 'group_admin',
							'rules' => 'trim|required' 
					),
					array (
							'field' => 'users_to_add',
							'label' => 'users_to_add',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			$this->form_validation->set_message ( 'is_unique', 'The %s is already taken' );
			
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				// $countVote = $this->checkUserPoll($data['poll_id'], $data['user_id']);
				
				$this->db->set ( 'created_date', 'NOW()', FALSE );
				$this->db->insert ( $this->table, $data );
				// echo $this->db->last_query(); die();
				$insert_id = $this->db->insert_id ();
				
				if ($insert_id) {
					// $array['user_id_fk'] = array($arr);
					
					foreach ( $arr as $val ) {
						$users_to_add = array (
								
								'group_id_fk' => $insert_id,
								'user_id_fk' => $val 
						);
						// $this->db->set ( 'created_date', 'NOW()', FALSE );
						$this->db->insert ( 'group_users', $users_to_add );
					}
					
					$GetDevice = $this->deviceType ( $arr );
					$msg = $Sendername . " has created new group";
					$Type = "Group";
					
					foreach ( $GetDevice as $device ) {
						
						if ($device ['device_name'] == 'android') {
							$devices = $device ['reg_id'];
							$this->Android_notification ( $arr, $Sendername, $userid, $msg, $Type, $insert_id, $devices );
						} else {
							$devices = $device ['reg_id_ios'];
							$this->Ios_notification ( $arr, $Sendername, $userid, $msg, $Type, $insert_id, $devices );
						}
					}
					
					$res = $this->getUserDetails ( $insert_id );
					// $group = $this->GetGroupIMG ( $insert_id );
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => "Success.. group created",
							'members_added' => $res 
					);
				}
			}
		} catch ( Exception $ex ) {
			// echo 123;
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function checkUserPoll($poll_id, $user_id) {
		$conditions = array (
				'user_unique_id' => $user_id,
				'poll_id' => $poll_id 
		);
		$this->db->where ( $conditions );
		$res = $this->db->get ( $this->table_poll_votes );
		if ($res->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function totalUsers() {
		$res = $this->db->get ( $this->user_table );
		return $res->num_rows ();
	}
	public function getUserDetails($users) {
		$this->db->where_in ( 'group_id_fk', $users );
		$this->db->select ( 'users.user_name,users.user_email,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		$this->db->from ( $this->table_group );
		$this->db->join ( $this->user_table, 'users.id =  group_users.user_id_fk' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function delete_group($id) {
		$this->db->where ( 'group_id', $id );
		$query = $this->db->delete ( $this->table );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function checkmembers($groupid, $arr) {
		$this->db->where ( 'group_id_fk', $groupid );
		$this->db->where_in ( 'user_id_fk', $arr );
		$res = $this->db->get ( $this->table_group );
		
		// echo $this->db->last_query(); die();
		$rows = $res->num_rows ();
		
		if ($rows === 0) {
			
			return true;
		}
	}
	public function add_Group_users($arr, $groupid) {
		foreach ( $arr as $val ) {
			$users_to_add = array (
					
					'group_id_fk' => $groupid,
					'user_id_fk' => $val 
			);
			$this->db->insert ( 'group_users', $users_to_add );
		}
		
		$res = $this->getUsersDetails ( $groupid, $arr );
		
		$message = array (
				'status' => true,
				'response_code' => '1',
				'message' => "Members Successfully added",
				'members_added' => $res 
		);
		
		return $message;
	}
	public function getUsersDetails($groupid, $arr) {
		$this->db->where_in ( 'user_id_fk', $arr );
		$this->db->where ( 'group_id_fk', $groupid );
		$this->db->select ( 'users.user_name,users.user_email,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		$this->db->from ( $this->table_group );
		$this->db->join ( $this->user_table, 'users.id =  group_users.user_id_fk' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function RemoveGroupMember($groupid, $userid) {
		$this->db->where ( 'user_id_fk', $userid );
		$this->db->where ( 'group_id_fk', $groupid );
		$query = $this->db->delete ( $this->table_group );
		
		// echo $this->db->last_query(); die();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function checkGroupmembers($groupid, $userid) {
		$this->db->where ( 'group_id_fk', $groupid );
		$this->db->where ( 'user_id_fk', $userid );
		$res = $this->db->get ( $this->table_group );
		
		// echo $this->db->last_query(); die();
		$rows = $res->num_rows ();
		
		if ($rows > 0) {
			
			return $rows;
		}
	}
	public function AddGroupPost($data, $groupid, $userslist, $userid) {
		$this->db->set ( 'created_date', 'NOW()', FALSE );
		$post = $this->db->insert ( $this->table_post, $data );
		$insert_id = $this->db->insert_id ();
		
		if ($post) {
			
			$GroupDetails = $this->GroupDetails ( $groupid );
			
			$GetDevice = $this->deviceType ( $userslist );
			$Sendername = $this->GetUserName ( $userid );
			
			$msg = $Sendername . " is posted in the group";
			$Type = "GroupPost";
			
			foreach ( $GetDevice as $device ) {
				
				if ($device ['device_name'] == 'android') {
					$devices = $device ['reg_id'];
					$this->Android_notification ( $arr, $Sendername, $userid, $msg, $Type, $GroupDetails, $devices );
				} else {
					$devices = $device ['reg_id_ios'];
					$this->Ios_notification ( $arr, $Sendername, $userid, $msg, $Type, $GroupDetails, $devices );
				}
			}
			
			$res = $this->getPostDetails ( $insert_id );
			
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => "Post Successfully added",
					'post_added' => $res 
			);
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function getPostDetails($insert_id) {
		$this->db->where ( 'post_id', $insert_id );
		$this->db->select ( "group_post.*,users.user_name,users.user_pic,count(distinct(activity_likes.like_id)) as likes_count ,count(distinct(activity_comments.comment_id)) as comment_count,IFNULL(activity_likes.is_liked,0) as is_liked ,activity_comments.poster_type" );
		$this->db->from ( $this->table_post );
		$this->db->join ( $this->user_table, 'group_post.user_id_fk = users.id', 'left' );
		$this->db->join ( $this->Likes, 'group_post.post_id = activity_likes.poster_id and activity_likes.type ="Group"', 'left' );
		$this->db->join ( $this->Comments, 'group_post.post_id = activity_comments.poster_id and activity_comments.type ="Group"', 'left' );
		$this->db->group_by ( 'activity_likes.poster_id' );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	public function UpdatePost($Postid, $data) {
		$this->db->set ( 'modified_date', 'NOW()', FALSE );
		$this->db->where ( 'post_id', $Postid );
		$query = $this->db->update ( $this->table_post, $data );
		
		if ($query) {
			$res = $this->getUpdatedPostDetails ( $Postid );
			
			if ($res) {
				
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Post Successfully Updated",
						'post_added' => $res 
				);
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'ID Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function getUpdatedPostDetails($Postid) {
		$this->db->where ( 'post_id', $Postid );
		$res = $this->db->get ( $this->table_post );
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function RemoveGroupPost($groupid, $postid) {
		$this->db->where ( 'post_id', $postid );
		$this->db->where ( 'group_id_fk', $groupid );
		$query = $this->db->delete ( $this->table_post );
		
		// echo $this->db->last_query(); die();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function GetAllUsers($userId) {
		$this->db->order_by ( "user_name", "asc" );
		$this->db->where_not_in ( 'id', $userId );
		$this->db->where_not_in ( 'id', '1' );
		$this->db->select ( 'users.*,IF(users.user_pic="" ,"null", CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic )) user_pic' );
		$this->db->from ( $this->user_table );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		
		return $res->result_array ();
	}
	public function GetAllGroups() {
		// $this->db->where( 'group_id_fk', $groupid );
		$this->db->select ( 'groups.*,IF(groups.photo_name="" ,"null", CONCAT("' . base_url ( 'uploads/gallery/' ) . '/",groups.photo_name )) photo_name,users.user_name as groupAdminname,COUNT(user_id_fk) as users_count' );
		$this->db->from ( $this->table_group );
		$this->db->join ( $this->table, 'groups.group_id =  group_users.group_id_fk' );
		$this->db->join ( $this->user_table, 'groups.group_admin =  users.id' );
		$this->db->group_by ( 'group_id_fk' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function SeeGroupMembers($groupid) {
		
		// echo 123;
		// exit;
		$this->db->where ( 'group_id', $groupid );
		$this->db->select ( 'users.*' );
		$this->db->from ( $this->table );
		$this->db->join ( $this->table_group, 'groups.group_id = group_users.group_id_fk' );
		$this->db->join ( $this->user_table, 'group_users.user_id_fk = users.id' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result ();
	}
	public function SeeGroupPosts($groupid, $userid, $start_from) {
		$response = array ();
		/*
		 * $query = "SELECT `group_post`.`post_id`, `activity_comments`.`body`, `users`.`id`, `users`.`user_name`, `users`.`user_pic`
		 * FROM `group_post`
		 * LEFT JOIN `activity_comments` ON `group_post`.`post_id` =`activity_comments`.`poster_id`
		 * LEFT JOIN `users` ON `activity_comments`.`user_id` =`users`.`id`
		 * WHERE `group_id_fk` = '67'
		 * ORDER BY `creation_date` ASC
		 * LIMIT 4";
		 */
		$this->db->limit ( 10, $start_from );
		$this->db->where ( 'group_id_fk', $groupid );
		$this->db->select ( 'group_post.*,users.user_name,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic,count(distinct(activity_likes.like_id)) as likes_count ,count(distinct(activity_comments.comment_id)) as comment_count,activity_likes.poster_id,activity_comments.poster_type' );
		$this->db->from ( $this->table_post );
		$this->db->join ( $this->user_table, 'group_post.user_id_fk = users.id', 'left' );
		$this->db->join ( $this->Likes, 'group_post.post_id = activity_likes.poster_id and activity_likes.type = "Group"', 'left' );
		$this->db->join ( $this->Comments, 'group_post.post_id = activity_comments.poster_id and activity_comments.type = "Group"', 'left' );
		$this->db->group_by ( 'group_post.post_id' );
		$res = $this->db->get ();
		
		// echo $this->db->last_query(); die();
		$result = $res->result_array ();
		// $i =0;
		foreach ( $result as $row ) {
			// print_r($row);
			$postID = $row ['post_id'];
			$posterID = $row ['poster_id'];
			// $row["comments"] = (object) array();
			
			$val = $this->getLikeStatus ( $posterID, $userid );
			
			if ($val) {
				$row ["is_liked"] = '1';
			} 

			else {
				$row ["is_liked"] = '0';
			}
			
			$row ["comments"] = ( object ) ( array ) $this->getAllComments ( $groupid, $postID );
			array_push ( $response, $row );
			// $i++;
			// print_r($response);
		}
		
		return $response;
	}
	public function AddLikes($data, $UserName, $activity_Type, $userid) {
		$this->db->set ( 'created_date', 'NOW()', FALSE );
		$post = $this->db->insert ( $this->Likes, $data );
		
		$postid = $data ['poster_id'];
		
		$like_id = $this->db->insert_id ();
		
		if ($post) {
			
			$GetPosterID = $this->PosterDetails ( $postid, $activity_Type );
			$receiverDetails = $this->ReceiverDetails ( $userid );
			
			// print_r($receiverDetails); die();
			
			$GetDeviceType = $GetPosterID->device_name;
			$posterid = $GetPosterID->id;
			
			if ($activity_Type == 'Group') {
				$groupid = $GetPosterID->group_id_fk;
				$Details = $this->GroupDetails ( $groupid );
				$likeType = 'group post';
			}
			
			if ($activity_Type == 'wall') {
				
				$wallid = $GetPosterID->wall_id;
				$Details = $this->getUpdatedWall ( $wallid );
				$likeType = 'wall post';
			}
			
			if ($activity_Type == 'home') {
				
				$actid = $GetPosterID->activity_id;
				$Details = $this->getHomePost ( $actid );
				$likeType = 'post';
			}
			
			$msg = $UserName . " likes your $likeType ";
			$Type = "Like";
			
			if ($GetDeviceType == 'android') {
				
				$deviceID = $GetPosterID->reg_id;
				$this->Android_notification_like ( $posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $likeType, $GetPosterID, $receiverDetails );
			} 

			else {
				
				// print_r($msg); die();
				
				$deviceID = $GetPosterID->reg_id_ios;
				$this->Ios_notification_like ( $posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $likeType, $GetPosterID, $receiverDetails );
			}
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => " You liked this post" 
			);
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function AddComments($data, $UserName, $activity_Type, $userid) {
		// $type = $this->input->post ( 'activity_type' );
		
		// print_r($type); die();
		if ($activity_Type == 'home') {
			$data ['group_id'] = 0;
		}
		
		if ($activity_Type == 'Project') {
			$data ['group_id'] = 0;
		}
		$postid = $data ['poster_id'];
		$this->db->set ( 'creation_date', 'NOW()', FALSE );
		$post = $this->db->insert ( $this->Comments, $data );
		$comment_id = $this->db->insert_id ();
		
		// print_r($res); die();
		
		if ($post) {
			// 
			$GetPosterID = $this->PosterDetails ( $postid, $activity_Type );
			
			$GetDeviceType = $GetPosterID->device_name;
			$posterid = $GetPosterID->id;
		//	echo 123; die();
			if ($activity_Type == 'Group') {
				
				$groupid = $GetPosterID->group_id_fk;
				$PostType = 'Group';
				$Details = $this->GroupDetails ( $groupid );
				$CommentType = 'group post';
				// $PostDetails = $this->PostDetails($comment_id);
			}
			
			if ($activity_Type == 'wall') {
				$wallid = $GetPosterID->wall_id;
				$PostType = 'wall';
				$Details = $this->getUpdatedWall ( $wallid );
				$CommentType = 'wall post';
			}
			
			if ($activity_Type == 'home') {
				
				$actid = $GetPosterID->activity_id;
				$PostType = 'home';
				$Details = $this->getHomePost ( $actid );
				$CommentType = 'post';
			}
			
			/*
			 * if ($activity_Type == 'Project') {
			 *
			 * $actid = $GetPosterID->activity_id;
			 * $PostType = 'project';
			 * $res = $this->getAllCommentByID ( $comment_id );
			 * $Details = $this->getHomePost ( $actid );
			 * $CommentType = 'Project post';
			 * }
			 */
			
			$msg = $UserName . " has commented on your $CommentType ";
			$Type = "Comment";
			
			if ($GetDeviceType == 'android') {
				$deviceID = $GetPosterID->reg_id;
				$this->Android_notification_comment ( $this->input->post ( 'postid' ), $UserName, $userid, $msg, $Type, $deviceID, $Details, $PostType );
			} 

			else {
				$deviceID = $GetPosterID->reg_id_ios;
				$this->Ios_notification_comment ( $posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $PostType );
			}
			
			$res = $this->getAllCommentByID ( $comment_id );
			
			if ($res) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => " Your comment has successfully posted.",
						'activity_type' => $res->type,
						'comment_id' => $res->comment_id,
						'body' => $res->body,
						'user_id' => $res->id,
						'username' => $res->user_name,
						'user_pic' => $res->user_pic 
				);
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Something went wrong' 
				];
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function PosterDetails($postid, $activity_Type) {
		if ($activity_Type == 'Group') {
			$this->db->where ( 'post_id', $postid );
			$this->db->select ( 'users.*,group_post.group_id_fk' );
			$this->db->from ( $this->table_post );
			$this->db->join ( $this->user_table, 'group_post.user_id_fk = users.id' );
			$res = $this->db->get ();
		}
		
		if ($activity_Type == 'wall') {
			
			$this->db->where ( 'wall_id', $postid );
			$this->db->select ( 'users.*,users_wall.wall_id' );
			$this->db->from ( $this->wall );
			$this->db->join ( $this->user_table, 'users_wall.senderuser_id = users.id' );
			$res = $this->db->get ();
		}
		
		if ($activity_Type == 'home') {
			
			$this->db->where ( 'activity_id', $postid );
			$this->db->select ( 'users.*,activities.activity_id' );
			$this->db->from ( $this->act );
			$this->db->join ( $this->user_table, 'activities.user_id = users.id' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		}
		
		if ($activity_Type == 'Project') {
				
			$this->db->where ( 'post_id', $postid );
			$this->db->select ( 'users.*,project_post.post_id' );
			$this->db->from ( 'project_post' );
			$this->db->join ( $this->user_table, 'project_post.user_id_fk = users.id' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		}
		
		return $res->row ();
	}
	public function getAllCommentByID($comment_id) {
		//print_r ( $comment_id ); // die();
		$this->db->where ( 'activity_comments.comment_id', $comment_id );
		$this->db->select ( 'activity_comments.comment_id,activity_comments.body,activity_comments.type,users.id,users.user_name,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		// $this->db->select ('activity_comments.body' );
		$this->db->from ( $this->Comments );
		$this->db->join ( $this->user_table, 'activity_comments.user_id =users.id', 'left' );
		$res = $this->db->get ();
		
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	public function getAllComments($groupid, $postid) {
		$this->db->limit ( 4 );
		$this->db->order_by ( "creation_date", "asc" );
		$this->db->where ( 'group_id_fk', $groupid );
		$this->db->where ( 'group_post.post_id', $postid );
		$this->db->select ( 'group_post.post_id , activity_comments.comment_id,activity_comments.body,users.id,users.user_name , IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		// $this->db->select ('activity_comments.body' );
		$this->db->from ( $this->table_post );
		$this->db->join ( $this->Comments, 'group_post.post_id =activity_comments.poster_id', 'left' );
		$this->db->join ( $this->user_table, 'activity_comments.user_id =users.id', 'left' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function UploadGalleryPhoto() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'gallery/';
		$config ['allowed_types'] = '*'; // 'mp3|wav';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		
		// print_r($config['file_name']);
		// exit;
		
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'photo_name' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	public function GetGroupAdmin($id) {
		$this->db->where ( 'groups.group_id', $id );
		$this->db->select ( 'groups.group_admin' );
		$this->db->from ( $this->table );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->row ()->group_admin;
	}
	public function GetUserName($userid) {
		
		// print_r($userid); die();
		$this->db->where ( 'id', $userid );
		$this->db->select ( 'user_name' );
		$this->db->from ( $this->user_table );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->row ()->user_name;
	}
	public function checkPost($postid) {
		$this->db->where ( 'post_id', $postid );
		$res = $this->db->get ( $this->table_post );
		
		// echo $this->db->last_query(); die();
		$rows = $res->num_rows ();
		
		if ($rows > 0) {
			
			return true;
		}
	}
	public function GetCommentsByPostID($postid, $type) {
		$this->db->where ( 'poster_id', $postid );
		$this->db->where ( 'type', $type );
		$this->db->select ( 'activity_comments.*,users.id,users.user_name,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		$this->db->from ( $this->Comments );
		$this->db->join ( $this->user_table, 'activity_comments.user_id =users.id', 'left' );
		$res = $this->db->get ();
		
		// echo $this->db->last_query ();
		// die ();
		return $res->result_array ();
	}
	public function RemoveComments($commentID, $postID) {
		$this->db->where ( 'comment_id', $commentID );
		$this->db->where ( 'poster_id', $postID );
		$query = $this->db->delete ( $this->Comments );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function GetGroupsByID($userid) {
		$response = array ();
		$this->db->where ( 'user_id_fk', $userid );
		$this->db->select ( 'groups.*,users.user_name as groupAdminname,IF(groups.photo_name="","null",CONCAT("' . base_url ( 'uploads/gallery/' ) . '/",groups.photo_name))photo_name' );
		$this->db->from ( $this->table_group );
		$this->db->join ( $this->table, 'groups.group_id =  group_users.group_id_fk', 'inner' );
		$this->db->join ( $this->user_table, 'groups.group_admin =  users.id', 'inner' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		$data = $res->result_array ();
		
		foreach ( $data as $row ) {
			
			$groupid = $row ['group_id'];
			
			$value = $this->getCount ( $groupid );
			
			foreach ( $value as $val ) {
				$row ['users_count'] = $val ['users_count'];
			}
			
			array_push ( $response, $row );
		}
		
		return $response;
	}
	public function getCount($groupid) {
		$this->db->where ( 'groups.group_id', $groupid );
		$this->db->select ( 'COUNT(user_id_fk) as users_count' );
		$this->db->from ( $this->table_group );
		$this->db->join ( $this->table, 'groups.group_id =  group_users.group_id_fk' );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	public function UpdateGroups($groupid, $data) {
		$this->db->set ( 'modification_date', 'NOW()', FALSE );
		$this->db->where ( 'group_id', $groupid );
		$query = $this->db->update ( $this->table, $data );
		
		if ($query) {
			$res = $this->getUpdatedGroupDetails ( $groupid );
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => "Group Successfully Updated",
					'group_id' => $res->group_id,
					'group_name' => $res->group_name,
					'group_desc' => $res->group_desc,
					'photo_name' => base_url ( 'uploads/gallery/' ) . '/' . $res->photo_name,
					'group_admin' => $res->group_admin,
					'created_date' => $res->created_date,
					'modification_date' => $res->modification_date,
					'status' => $res->status,
					'privacy' => $res->privacy 
			);
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function getUpdatedGroupDetails($groupid) {
		$this->db->where ( 'group_id', $groupid );
		$res = $this->db->get ( $this->table );
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	public function getLikeStatus($posterID, $userid) {
		$this->db->where ( 'poster_id', $posterID );
		$this->db->where ( 'user_id', $userid );
		$this->db->where ( 'type', 'Group' );
		$res = $this->db->get ( $this->Likes );
		$rows = $res->num_rows ();
		
		if ($rows == 1) {
			
			return true;
		}
	}
	function Android_notification($arr, $Sendername, $userid, $msg, $Type, $GroupDetails, $devices) {
		
		// print_r($devices); die();
		// $this->db->where_in ( 'id', $arr );
		// $this->db->select ( 'reg_id' );
		// $query = $this->db->get ( 'users' );
		
		// // echo $this->db->last_query();die;
		// $getuser = $query->result_array ();
		
		// /*
		// * if ($deviceID != '') {
		// * $ids = array('android_device_id' => $deviceID);
		// * $getuser = array($ids);
		// * }
		// */
		// foreach ( $getuser as $val ) {
		// $register_send = $val;
		// $value = $register_send ['reg_id'];
		$get = json_decode ( $devices );
		/*
		 * $data['data'] = array(
		 * 'type' => 'Poke',
		 *
		 * );
		 */
		// print_r($get);
		
		// $notification = GetNonActionNotificationDetails($action);
		// $message = 'Hi i am sending push notification';
		$api_key = 'AIzaSyDCVGdZc9Abs5cRWPCMWWiKejz9W3hX-wE';
		
		if (empty ( $api_key ) || count ( $get ) < 0) {
			$result = array (
					'success' => '0',
					'message' => 'api or reg id not found' 
			);
			echo json_encode ( $result );
			die ();
		}
		$registrationIDs = $get;
		
		$message = $msg;
		$url = 'https://android.googleapis.com/gcm/send';
		$resultData = array (
				"type" => $Type,
				"title" => 'Notification of Group',
				"message" => $message,
				"users" => $arr,
				"poster_id" => $userid,
				"group" => $GroupDetails 
		);
		
		// print_r($resultData); die();
		
		$fields = array (
				'registration_ids' => $registrationIDs,
				'data' => array (
						"message" => $message,
						"title" => 'Notification of Group',
						"type" => $Type,
						'data' => $resultData 
				) 
		);
		// print_r($fields); die();
		$headers = array (
				'Authorization: key=' . $api_key,
				'Content-Type: application/json' 
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		if ($result) {
			// echo good;
		} else {
			// die('Curl failed: ' . curl_error($ch));
		}
		curl_close ( $ch );
		// print_r ( $result );
		
		// die();
	}
	function Ios_notification($arr, $Sendername, $userid, $msg, $Type, $GroupDetails) {
		set_time_limit ( 0 );
		
		// charset header for output
		header ( 'content-type: text/html; charset: utf-8' );
		
		// this is the pass phrase you defined when creating the key
		$passphrase = '123456';
		
		// you can post a variable to this string or edit the message here
		if (! isset ( $_POST ['msg'] )) {
			$_POST ['msg'] = $msg;
		}
		
		// tr_to_utf function needed to fix the Turkish characters
		$message = $_POST ['msg'];
		
		// load your device ids to an array
		
		$this->db->where_in ( 'id', $arr );
		$this->db->select ( 'reg_id_ios' );
		$query = $this->db->get ( 'users' );
		$getuser = $query->result_array ();
		/*
		 * $item = array (
		 * 'f9c68652e479664827af1c9892e04c2c6b8398561f7e34e3d9f0125f58fc94f1'
		 * );
		 */
		
		// this is where you can customize your notification
		$payload = '{"aps":{
		"alert":"' . $message . '",
		"sound":"default",
		"type": " ' . $Type . '",
		"group":"' . $GroupDetails . '"
}}';
		
		$result = 'Start' . '<br />';
		
		// //////////////////////////////////////////////////////////////////////////////
		// start to create connection
		$ctx = stream_context_create ();
		stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'apnsdev.pem' );
		stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
		
		// echo count ( $deviceIds ) . ' devices will receive notifications.<br />';
		
		foreach ( $getuser as $val ) {
			
			$register_send = $val;
			$value = $register_send ['reg_id_ios'];
			
			$get = json_decode ( $value );
			
			// print_r($register_send); //exit;
			
			foreach ( $get as $val ) {
				
				// print_r($val);
				// wait for some time
				sleep ( 1 );
				
				// Open a connection to the APNS server
				$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
				
				if (! $fp) {
					// exit ( "Failed to connect: $err $errstr" . '<br />' );
				} else {
					// echo 'Apple service is online. ' . '<br />';
				}
				
				/*
				 * $payload['aps'] = array(
				 * 'alert' => $message,
				 * 'badge' => $badge,
				 * 'sound' => 'default'
				 * );
				 * $body['data'] = $data;
				 */
				
				// Encode the payload as JSON
				// $payload = json_encode($body);
				
				// Build the binary notification
				$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $val ) . pack ( 'n', strlen ( $payload ) ) . $payload;
				
				// Send it to the server
				$result = fwrite ( $fp, $msg, strlen ( $msg ) );
				
				if (! $result) {
					// echo 'Undelivered message count: ' . $item . '<br />';
				} else {
					// echo 'Delivered message count: ' . $item . '<br />';
				}
				
				if ($fp) {
					fclose ( $fp );
					// echo 'The connection has been closed by the client' . '<br />';
				}
			}
			
			// echo count($deviceIds) . ' devices have received notifications.<br />';
			
			// function for fixing Turkish characters
			/*
			 * function tr_to_utf($text) {
			 * $text = trim ( $text );
			 * $search = array (
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�',
			 * '�'
			 * );
			 * $replace = array (
			 * 'Ü',
			 * 'Ş',
			 * '&#286;�',
			 * 'Ç',
			 * 'İ',
			 * 'Ö',
			 * 'ü',
			 * 'ş',
			 * 'ğ',
			 * 'ç',
			 * 'ı',
			 * 'ö'
			 * );
			 * $new_text = str_replace ( $search, $replace, $text );
			 * return $new_text;
			 * }
			 */
			
			// set time limit back to a normal value
			set_time_limit ( 30 );
		}
	}
	public function updateGroupAdmin($groupid, $value) {
		$this->db->set ( 'modification_date', 'NOW()', FALSE );
		$this->db->where ( 'group_id', $groupid );
		$update = $this->db->update ( $this->table, $value );
		
		if ($update) {
			return true;
		}
	}
	public function deviceType($arr) {
		$this->db->where_in ( 'id', $arr );
		$res = $this->db->get ( $this->user_table );
		
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function GetGroupUsers($groupid) {
		$this->db->where ( 'group_users.group_id_fk', $groupid );
		$this->db->select ( 'group_users.user_id_fk' );
		$this->db->from ( $this->table_group );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result ();
	}
	public function Android_notification_like($posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $likeType, $GetPosterID, $receiverDetails) {
		$get = json_decode ( $deviceID );
		
		// print_r($deviceID); die();
		
		/*
		 * $data['data'] = array(
		 * 'type' => 'Poke',
		 *
		 * );
		 */
		// print_r($get);
		
		// $notification = GetNonActionNotificationDetails($action);
		// $message = 'Hi i am sending push notification';
		$api_key = 'AIzaSyDCVGdZc9Abs5cRWPCMWWiKejz9W3hX-wE';
		
		if (empty ( $api_key ) || count ( $get ) < 0) {
			$result = array (
					'success' => '0',
					'message' => 'api or reg id not found' 
			);
			echo json_encode ( $result );
			die ();
		}
		$registrationIDs = $get;
		
		$message = $msg;
		$url = 'https://android.googleapis.com/gcm/send';
		$resultData = array (
				"type" => $Type,
				"title" => 'Notification of Like',
				"message" => $message,
				"poster_id" => $userid,
				"PostType" => $likeType,
				"group" => $Details,
				"users" => $GetPosterID,
				"receiveruser" => $receiverDetails 
		);
		
		// print_r($resultData); die();
		
		$fields = array (
				'registration_ids' => $registrationIDs,
				'data' => array (
						"message" => $message,
						"title" => 'Notification of Like',
						"type" => $Type,
						'data' => $resultData 
				) 
		);
		// print_r($fields); die();
		$headers = array (
				'Authorization: key=' . $api_key,
				'Content-Type: application/json' 
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		// print_r ( $result );
	}
	public function Ios_notification_like($posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $likeType, $GetPosterID, $receiverDetails) {
		
		// print_r($msg); die();
		set_time_limit ( 0 );
		
		// charset header for output
		header ( 'content-type: text/html; charset: utf-8' );
		
		// this is the pass phrase you defined when creating the key
		$passphrase = '123456';
		
		// you can post a variable to this string or edit the message here
		if (! isset ( $_POST ['msg'] )) {
			$_POST ['msg'] = $msg;
		}
		
		// tr_to_utf function needed to fix the Turkish characters
		$message = $_POST ['msg'];
		
		// print_r($message); die();
		
		// load your device ids to an array
		
		/*
		 * $item = array (
		 * 'f9c68652e479664827af1c9892e04c2c6b8398561f7e34e3d9f0125f58fc94f1'
		 * );
		 */
		
		// this is where you can customize your notification
		$payload = '{"aps":{
		"alert":"' . $message . '",
	    "sound":"default",
		"type":"' . $Type . '",
		"group": "' . $Details . '"
}}';
		
		$result = 'Start' . '<br />';
		
		// //////////////////////////////////////////////////////////////////////////////
		// start to create connection
		$ctx = stream_context_create ();
		stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'apnsdev.pem' );
		stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
		
		// echo count ( $deviceIds ) . ' devices will receive notifications.<br />';
		
		$get = json_decode ( $deviceID );
		
		// print_r($get); exit;
		
		foreach ( $get as $val ) {
			
			// print_r($val);
			// wait for some time
			sleep ( 1 );
			
			// Open a connection to the APNS server
			$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
			
			if (! $fp) {
				// exit ( "Failed to connect: $err $errstr" . '<br />' );
			} else {
				// echo 'Apple service is online. ' . '<br />';
			}
			
			/*
			 * $payload['aps'] = array(
			 * 'alert' => $message,
			 * 'badge' => $badge,
			 * 'sound' => 'default'
			 * );
			 * $body['data'] = $data;
			 */
			
			// Encode the payload as JSON
			// $payload = json_encode($body);
			
			// Build the binary notification
			$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $val ) . pack ( 'n', strlen ( $payload ) ) . $payload;
			
			// Send it to the server
			$result = fwrite ( $fp, $msg, strlen ( $msg ) );
			
			if (! $result) {
				// echo 'Undelivered message count: ' . $item . '<br />';
			} else {
				// echo $payload;
			}
			
			if ($fp) {
				fclose ( $fp );
				// echo 'The connection has been closed by the client' . '<br />';
			}
		}
		
		// echo count($deviceIds) . ' devices have received notifications.<br />';
		
		// function for fixing Turkish characters
		/*
		 * function tr_to_utf($text) {
		 * $text = trim ( $text );
		 * $search = array (
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�'
		 * );
		 * $replace = array (
		 * 'Ü',
		 * 'Ş',
		 * '&#286;�',
		 * 'Ç',
		 * 'İ',
		 * 'Ö',
		 * 'ü',
		 * 'ş',
		 * 'ğ',
		 * 'ç',
		 * 'ı',
		 * 'ö'
		 * );
		 * $new_text = str_replace ( $search, $replace, $text );
		 * return $new_text;
		 * }
		 */
		
		// set time limit back to a normal value
		set_time_limit ( 30 );
	}
	public function GroupDetails($groupid) {
		$this->db->where ( 'groups.group_id', $groupid );
		$this->db->select ( "groups.*,users.user_name as groupAdminName" );
		$this->db->from ( $this->table );
		$this->db->join ( $this->user_table, 'groups.group_admin=users.id' );
		$res = $this->db->get ();
		
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	
	/*
	 * public function PostDetails($comment_id)
	 * {
	 * $this->db->where('activity_comments.comment_id',$comment_id);
	 * $this->db->select("activity_comments.poster_id");
	 * $this->db->from ( $this->Comments);
	 * $this->db->join($this->table_post,'activity_comments.group_id=group_post.group_id_fk');
	 * $res = $this->db->get();
	 *
	 * echo $this->db->last_query(); die();
	 * return $res->row();
	 * }
	 */
	public function Android_notification_comment($posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $PostType) {
		$get = json_decode ( $deviceID );
		
		// print_r($deviceID); die();
		
		/*
		 * $data['data'] = array(
		 * 'type' => 'Poke',
		 *
		 * );
		 */
		// print_r($get);
		
		// $notification = GetNonActionNotificationDetails($action);
		// $message = 'Hi i am sending push notification';
		$api_key = 'AIzaSyDCVGdZc9Abs5cRWPCMWWiKejz9W3hX-wE';
		
		if (empty ( $api_key ) || count ( $get ) < 0) {
			$result = array (
					'success' => '0',
					'message' => 'api or reg id not found' 
			);
			echo json_encode ( $result );
			die ();
		}
		$registrationIDs = $get;
		
		$message = $msg;
		$url = 'https://android.googleapis.com/gcm/send';
		$resultData = array (
				"type" => $Type,
				"title" => 'Notification of Group',
				"message" => $message,
				"poster_id" => $userid,
				"postid" => $posterid,
				"PostType" => $PostType,
				"group" => $Details 
		);
		
		// print_r($resultData); die();
		
		$fields = array (
				'registration_ids' => $registrationIDs,
				'data' => array (
						"message" => $message,
						"title" => 'Notification of Group',
						"type" => $Type,
						'data' => $resultData 
				) 
		);
		// print_r($fields); die();
		$headers = array (
				'Authorization: key=' . $api_key,
				'Content-Type: application/json' 
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		// print_r ( $result );
	}
	public function Ios_notification_comment($posterid, $UserName, $userid, $msg, $Type, $deviceID, $Details, $PostType) {
		
		// print_r($msg); die();
		set_time_limit ( 0 );
		
		// charset header for output
		header ( 'content-type: text/html; charset: utf-8' );
		
		// this is the pass phrase you defined when creating the key
		$passphrase = '123456';
		
		// you can post a variable to this string or edit the message here
		if (! isset ( $_POST ['msg'] )) {
			$_POST ['msg'] = $msg;
		}
		
		// tr_to_utf function needed to fix the Turkish characters
		$message = $_POST ['msg'];
		
		// print_r($message); die();
		
		// load your device ids to an array
		
		/*
		 * $item = array (
		 * 'f9c68652e479664827af1c9892e04c2c6b8398561f7e34e3d9f0125f58fc94f1'
		 * );
		 */
		
		// this is where you can customize your notification
		$payload = '{"aps":{
		"alert":"' . $message . '",
		"sound":"default",
		"type":"' . $Type . '",
		"postid":"' . $posterid . '",
		"PostType":"' . $PostType . '",
		"group":"' . $Details . '"
}}';
		
		$result = 'Start' . '<br />';
		
		// //////////////////////////////////////////////////////////////////////////////
		// start to create connection
		$ctx = stream_context_create ();
		stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'apnsdev.pem' );
		stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
		
		// echo count ( $deviceIds ) . ' devices will receive notifications.<br />';
		
		$get = json_decode ( $deviceID );
		
		// print_r($get); exit;
		
		foreach ( $get as $val ) {
			
			// print_r($val);
			// wait for some time
			sleep ( 1 );
			
			// Open a connection to the APNS server
			$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
			
			if (! $fp) {
				// exit ( "Failed to connect: $err $errstr" . '<br />' );
			} else {
				// echo 'Apple service is online. ' . '<br />';
			}
			
			/*
			 * $payload['aps'] = array(
			 * 'alert' => $message,
			 * 'badge' => $badge,
			 * 'sound' => 'default'
			 * );
			 * $body['data'] = $data;
			 */
			
			// Encode the payload as JSON
			// $payload = json_encode($body);
			
			// Build the binary notification
			$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $val ) . pack ( 'n', strlen ( $payload ) ) . $payload;
			
			// Send it to the server
			$result = fwrite ( $fp, $msg, strlen ( $msg ) );
			
			if (! $result) {
				// echo 'Undelivered message count: ' . $item . '<br />';
			} else {
				// echo $payload;
			}
			
			if ($fp) {
				fclose ( $fp );
				// echo 'The connection has been closed by the client' . '<br />';
			}
		}
		
		// echo count($deviceIds) . ' devices have received notifications.<br />';
		
		// function for fixing Turkish characters
		/*
		 * function tr_to_utf($text) {
		 * $text = trim ( $text );
		 * $search = array (
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�'
		 * );
		 * $replace = array (
		 * 'Ü',
		 * 'Ş',
		 * '&#286;�',
		 * 'Ç',
		 * 'İ',
		 * 'Ö',
		 * 'ü',
		 * 'ş',
		 * 'ğ',
		 * 'ç',
		 * 'ı',
		 * 'ö'
		 * );
		 * $new_text = str_replace ( $search, $replace, $text );
		 * return $new_text;
		 * }
		 */
		
		// set time limit back to a normal value
		set_time_limit ( 30 );
	}
	public function getUpdatedWall($wall_id) {
		$this->db->where ( 'wall_id', $wall_id );
		$res = $this->db->get ( $this->wall );
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	public function getHomePost($actid) {
		$this->db->where ( 'activity_id', $actid );
		$res = $this->db->get ( 'activities' );
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	public function ReceiverDetails($userid) {
		$this->db->where ( 'id', $userid );
		$this->db->select ( 'users.*,IF(users.user_pic="" ,"null", CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic )) user_pic' );
		$this->db->from ( 'users' );
		$res = $this->db->get ();
		return $res->row ();
		// echo $this->db->last_query(); die();
	}
}

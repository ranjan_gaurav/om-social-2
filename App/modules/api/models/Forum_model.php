<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 10/13/2015
 * Time: 3:36 PM
 */
class Forum_model extends CI_Model {

    var $forum_table = "nc_forum";
    var $comments_table = "nc_comments";

    /**
     * Load Constructor Model
     */
    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Tips data
     */
    public function getAllForums($ID = null) {
        if ($ID != null) {
            $this->db->where('forum_id', $ID);
        }
        //$this->db->where('is_active', '1');
        $this->db->select('for.*,IF((`us`.`user_pic`=""),"null",CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic');
        $this->db->from($this->forum_table . ' as for');
        $this->db->join('users' . " us", "for.user_id = us.id", 'left');
        $result = $this->db->get();
        return $result->result();
    }

    public function getUserPic($id) {
        $this->db->select('user_pic');
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        return $query->row();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Tips data
     */
    public function getAllComments($ID = null) {
        if ($ID != null) {
            $this->db->where('com.forum_id', $ID);
        }
        //$this->db->select('com.*,us.user_nicename,us.user_nicename,IF(ISNULL(us.user_pic),0,CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic', FALSE);
        $this->db->select('com.*,us.user_name,IF((`us`.`user_pic`=""),null,CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic', FALSE);
        $this->db->from('nc_comments' . ' as com');
        $this->db->join('users' . " us", "com.user_id = us.id", 'left');

        $result = $this->db->get();
        //print_r($this->db->last_query());
        //die;
        return $result->result();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 13/10/15
     * @description : get product data
     */
    public function getProducts($data = null) {
        try {
            if ($ID != null && $ID != 0) {
                $this->db->where('material_id', $ID);
            }
            if (!empty($data)) {
                if (isset($data['id'])) {
                    $this->db->where('material_id', $data['id']);
                } else if (isset($data['barcode'])) {
                    $this->db->where('barcode', $data['barcode']);
                }
            }
            $this->db->where('material_status', "1");
            $this->db->from($this->product_table);
            $result = $this->db->get();
            //echo $this->db->last_query();die;
            return $result->result();
        } catch (Exception $e) {
            log_message('Error', 'error =' . $e->getMessage());
            return false;
        }
    }

    /**
     * @method : add Product
     * @param $data
     * @return bool
     * @date : 13/10/15
     * @description : Add new product in the product table
     */
    public function addforum($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_id', 'label' => 'User id', 'rules' => 'required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('forum_creation_date', 'NOW()', FALSE);
                $this->db->insert($this->forum_table, $data);
                $insert_id = $this->db->insert_id();
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success",
                    'forum_id' => $insert_id
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    // add comments
    public function addcomments($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_id', 'label' => 'User id', 'rules' => 'required'),
                array('field' => 'forum_id', 'label' => 'Forum id', 'rules' => 'required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $forumid = $data['forum_id'];
                $this->db->set('comment_creation_date', 'NOW()', FALSE);
                $this->db->insert($this->comments_table, $data);
                $insert_id = $this->db->insert_id();
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success",
                    'forum_id' => $forumid,
                    'comments_id' => $insert_id
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     * @date : 13/10/15
     * @method : updateProduct
     * @description : To update product
     */
    public function update($id, $data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'barcode', 'label' => 'Barcode', 'rules' => 'required|min_length[10]'),
                array('field' => 'material_desc', 'label' => 'Material Desc', 'rules' => 'required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->where('material_id', $id);
                $this->db->set('material_modification_date', 'NOW()', FALSE);
                $query = $this->db->update($this->product_table, $data);
                $this->cimongo->update($this->product_table, $data, array('material_id' => $id));
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success"
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @param $id
     * @return bool
     * @date : 13/10/15
     * @method :  Delete Product
     * @description : To delete product from Product
     */
    public function delete($id) {
        if (isset($id) && $id != '') {
            $this->db->where('material_id', $id);
            $query = $this->db->delete($this->product_table);
            $this->cimongo->delete($this->product_table, array('material_id' => $id));
            if ($query) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

<?php

class Gallery_model extends CI_Model {

    var $gallery_table = "event_photos";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    Public function getAllGalleryPhotos() {
        $this->db->select('photo_id,if(photo_name="","null" ,CONCAT("' . base_url('uploads/gallery/') . '/",photo_name ) ) photo_name');
        $this->db->where('event_id', '0');
        $this->db->from($this->gallery_table);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function UploadGalleryPhoto() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 6000);
        ini_set('max_execution_time', 6000);
        $config['upload_path'] = IMAGESPATH . 'gallery/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('photo_name')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    public function AddGalleryPhoto($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d H:i:s');
        $data['creation_date'] = $date;
        $res = $this->db->insert($this->gallery_table, $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

}

<?php
class Activity_model extends CI_Model {
	var $activity_table = "activities";
	var $event_table = "events";
	var $comment_table = "activity_comments";
	var $post_like_table = "activity_likes";
	var $event_like_table = "event_likes";
	var $user_table = "users";
	var $comment = "activity_comments";
	function __construct() {
		parent::__construct ();
		date_default_timezone_set ( 'Asia/Kolkata' );
	}
	Public function getAllActivities($ID = null, $user_id,$start_from) {
		$response = array ();
		/*
		 * $this->db->select('act.activity_id,act.is_top,act.type,act.resource_id,us.user_name,act.body,act.description,act.creation_date,act.user_id,count(distinct(et.like_id)) as likes_count,et.poster_id,count(distinct(com.comment_id)) as comment_count,IF(us.user_pic="" ,"null",CONCAT("' . base_url('uploads/users/profile/') . '/",us.user_pic ) ) user_pic,IF(act.photo_id="" ,"null",CONCAT("' . base_url('uploads/activities/') . '/",act.photo_id ) ) photo_id,');
		 * if ($ID != null) {
		 * $this->db->where('act.activity_id', $ID);
		 * }
		 * $this->db->where('act.is_top', '0');
		 * $this->db->from($this->activity_table . ' act');
		 * $this->db->join($this->user_table . ' us', '`us`.`id`=`act`.`user_id`');
		 * $this->db->join($this->post_like_table . ' et', '`act`.`activity_id`=`et`.`poster_id` and et.type ="Home"','left');
		 * $this->db->join($this->comment . ' com', '`act`.`activity_id`=`com`.`poster_id` and com.type = "Home"','left');
		 * $this->db->group_by('act.activity_id');
		 * $res = $this->db->get();
		 */
		// echo $this->db->last_query(); die();
		$sql = ('
SELECT
`act`.`activity_id`, 
`act`.`is_top`,
`act`.`type`,
 0 as resource_id,
`us`.`user_name`,
 `act`.`body`, 
 `act`.`description`, 
 `act`.`creation_date`, 
 `act`.`user_id`,
  "" as  groupid,
 count(distinct(et.like_id)) as likes_count, 
 count(distinct(com.comment_id)) as comment_count,
 `et`.`poster_id`,
IF(us.user_pic="", "null", CONCAT("http://192.168.1.186/bcg/uploads/users/profile/", us.user_pic ) ) user_pic, IF(act.photo_id="", "null", CONCAT("http://192.168.1.186/bcg/uploads/activities/", act.photo_id ) ) photo_id
FROM `activities` `act`
JOIN `users` `us` ON `us`.`id`=`act`.`user_id`
LEFT JOIN `activity_likes` `et` ON `act`.`activity_id`=`et`.`poster_id` and `et`.`type` = "Home"
LEFT JOIN `activity_comments` `com` ON `act`.`activity_id`=`com`.`poster_id` and `com`.`type` = "Home"
WHERE `act`.`is_top` = "0"
GROUP BY `act`.`activity_id`

    			
UNION

SELECT `group_post`.post_id,
 0 as is_top,
 "grouppost" as type,
 0 as resource_id,
 `users`.`user_name`,
group_post.post_title,
group_post.post_description,
group_post.created_date as creation_date,
group_post.user_id_fk,
group_post.group_id_fk as groupid,

count(distinct(activity_likes.like_id)) as likes_count,
count(distinct(activity_comments.comment_id)) as comment_count,
`activity_likes`.`poster_id`,
IF(users.user_pic="", "null", CONCAT("http://192.168.1.186/bcg/uploads/users/profile/", users.user_pic ) ) user_pic, "null" as photo_id
 -- `activity_comments`.`poster_type`
FROM `group_post`
LEFT JOIN `users` ON `group_post`.`user_id_fk` = `users`.`id`
LEFT JOIN `activity_likes` ON `group_post`.`post_id` = `activity_likes`.`poster_id` and `activity_likes`.`type` = "Group"
LEFT JOIN `activity_comments` ON `group_post`.`post_id` = `activity_comments`.`poster_id` and `activity_comments`.`type` = "Group"
WHERE `user_id_fk` = ' . $user_id . '

GROUP BY `group_post`.`post_id`
ORDER BY  `creation_date` desc
Limit 10 offset '.$start_from.'
    
 ');
		$query = $this->db->query ( $sql );
		
	// echo $this->db->last_query(); die();
		$result = $query->result_array ();
		
		foreach ( $result as $row ) {
			// print_r($row); //die();
			
			$posterID = $row ['poster_id'];
			$ActType = $row ['type'];
			$val = $this->getLikeStatus ( $posterID, $user_id, $ActType );
			
			if ($val) {
				$row ["is_liked"] = '1';
			} 

			else {
				$row ["is_liked"] = '0';
			}
			
			array_push ( $response, $row );
		}
		
		return $response;
	}
	public function addActivity($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'user_id',
							'label' => 'UserID',
							'rules' => 'trim|required' 
					),
					array (
							'field' => 'body',
							'label' => 'Activity Text',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				date_default_timezone_set ( 'Asia/Kolkata' );
				$date = date ( 'Y-m-d H:i:s' );
				$data ['creation_date'] = $date;
				$data ['modification_date'] = $date;
				$data ['type'] = 'post';
				$query = $this->db->insert ( $this->activity_table, $data );
				$insert_id = $this->db->insert_id ();
				if (! empty ( $insert_id )) {
					if ($data ['user_id'] != '0') :
						$isLiked = '(SELECT count(`like_id`) FROM `activity_likes` WHERE `poster_id` = ' . $data ['user_id'] . ' AND resource_id=`act`.activity_id ) as is_liked';
					 else :
						$isLiked = "";
					endif;
					$this->db->select ( 'act.activity_id,us.user_name,act.body,act.creation_date,act.user_id,act.like_count,act.comment_count,IF(us.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic ) ) user_pic,IF(act.photo_id="" ,"null",CONCAT("' . base_url ( 'uploads/activities/' ) . '/",act.photo_id ) ) photo_id,' . $isLiked );
					$this->db->where ( 'activity_id', $insert_id );
					$this->db->from ( $this->activity_table . ' act' );
					$this->db->join ( $this->user_table . ' us', '`us`.`id`=`act`.`user_id`' );
					$last_insert_query = $this->db->get ();
					$last_data = $last_insert_query->result_array ();
				}
				if ($query) {
					$message = array (
							'status' => true,
							'response_code' => '1',
							'data' => $last_data 
					);
				} else {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'message' => rtrim ( $errors_array, ', ' ) 
					);
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function delete($id) {
		if (isset ( $id ) && $id != '') {
			$this->db->where ( 'activity_id', $id );
			$query = $this->db->delete ( $this->activity_table );
			if ($query) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function addLike($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'type',
							'label' => 'Please Define your type',
							'rules' => 'trim|required' 
					),
					array (
							'field' => 'resource_id',
							'label' => 'Please Send Resource',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$counttotal = $this->countTotalLikes ( $data ['poster_id'], $data ['resource_id'] );
				if ($counttotal > 0) {
					$message = array (
							'status' => false,
							'response_code' => '0',
							'messsage' => 'already liked',
							'total' => $counttotal 
					);
				} else {
					$this->db->where ( 'poster_id', $data ['poster_id'] );
					$this->db->where ( 'resource_id', $data ['resource_id'] );
					if ($data ['type'] == 'post') {
						$query = $this->db->get ( 'activity_likes' );
						$count_row = $query->num_rows ();
					}
					if ($data ['type'] == 'event') {
						$query = $this->db->get ( 'event_likes' );
						$count_row = $query->num_rows ();
					}
					/*
					 * if ($count_row > 0) {
					 * $message = array(
					 * 'status' => False,
					 * 'response_code' => '0',
					 * 'message' => 'Already Liked'
					 * );
					 * } else {
					 */
					if ($data ['type'] == 'post') {
						$table = $this->post_like_table;
					} elseif ($data ['type'] == 'event') {
						$table = $this->event_like_table;
					}
					$likeData = array (
							'resource_id' => $data ['resource_id'],
							'poster_id' => $data ['poster_id'] 
					);
					$query = $this->db->insert ( $table, $likeData );
					if ($query) {
						$message = array (
								'status' => true,
								'response_code' => '1' 
						);
					} else {
						$message = array (
								'status' => false,
								'response_code' => '0' 
						);
					}
				}
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function countTotalLikes($userID, $resId) {
		$this->db->where ( 'poster_id', $userID );
		$this->db->where ( 'resource_id', $resId );
		$this->db->from ( $this->post_like_table );
		$res = $this->db->get ();
		return $res->num_rows ();
	}
	public function incrementLike($data) {
		if ($data ['type'] == 'post') {
			$table = $this->activity_table;
			$condition = array (
					'activity_id' => $data ['resource_id'] 
			);
		} elseif ($data ['type'] == 'event') {
			$table = $this->event_table;
			$condition = array (
					'event_id' => $data ['resource_id'] 
			);
		}
		$totalLike = $this->db->select ( 'like_count' )->get_where ( $table, $condition )->row ()->like_count;
		$likes = $totalLike + 1;
		$Updatecount = array (
				'like_count' => $likes 
		);
		$updateLikes = $this->db->where ( $condition )->update ( $table, $Updatecount );
		if ($updateLikes) {
			return $likes;
		} else {
			return false;
		}
	}
	public function getComments() {
		/*
		 * $this->db->select('comm.resource_id as activity_id,comm.body,comm.creation_date,us.id as user_id,if(us.user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '/",us.user_pic ) ) user_pic,us.user_name');
		 * $this->db->from($this->comment_table . ' comm');
		 * $this->db->join($this->user_table . ' us', '`us`.`id`=`comm`.`poster_id`');
		 * $this->db->where_in('comment_id','SELECT max(comment_id) FROM `activity_comments` `comm` GROUP BY `resource_id`');
		 */
		$res = $this->db->query ( 'SELECT `comment_id`,`comm`.`user_id` as `activity_id`, `comm`.`body`, `comm`.`creation_date`, `us`.`id` as `user_id`, if(us.user_pic="", "null", CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic ) ) user_pic, `us`.`user_name` FROM `activity_comments` `comm` JOIN `users` `us` ON `us`.`id`=`comm`.`poster_id` WHERE `comment_id` IN(SELECT max(comment_id) FROM `activity_comments` `comm` GROUP BY `user_id`)' );
		// $res = $this->db->get();
		$comArray = array ();
		foreach ( $res->result_array () as $list ) {
			$ActID = $list ['activity_id'];
			$comArray [$ActID] = $list;
		}
		return $comArray;
	}
	public function getTopActivity($user_id) {
		$response = array ();
	//	$this->db->limit(10,$start_from);
		$this->db->select ( 'act.activity_id,act.is_top,act.type,act.resource_id,us.user_name,act.body,act.description,act.creation_date,act.user_id,0 as  groupid,count(distinct(et.like_id)) as likes_count,count(distinct(com.comment_id)) as comment_count,et.poster_id,IF(us.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",us.user_pic ) ) user_pic,IF(act.photo_id="" ,"null",CONCAT("' . base_url ( 'uploads/activities/' ) . '/",act.photo_id ) ) photo_id,' );
		$this->db->where ( 'act.is_top', '1' );
		$this->db->from ( $this->activity_table . ' act' );
		$this->db->join ( $this->user_table . ' us', '`us`.`id`=`act`.`user_id`' );
		$this->db->join ( $this->post_like_table . ' et', '`act`.`activity_id`=`et`.`poster_id` and et.type = "Home"', 'left' );
		$this->db->join ( $this->comment . ' com', '`act`.`activity_id`=`com`.`poster_id` and com.type = "Home"', 'left' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		$result = $res->result_array ();
		
		foreach ( $result as $row ) {
			$posterID = $row ['poster_id'];
			$val = $this->getLikeStatus ( $posterID, $user_id, 'Home' );
			
			if ($val) {
				$row ["is_liked"] = '1';
			} 

			else {
				$row ["is_liked"] = '0';
			}
			
			array_push ( $response, $row );
		}
		
		return $response;
	}
	public function getLikeStatus($posterID, $userid, $ActType) {
		$this->db->where ( 'poster_id', $posterID );
		$this->db->where ( 'user_id', $userid );
		if ($ActType == 'post') {
			$this->db->where ( 'type', 'Home' );
		}
		if ($ActType == 'grouppost') {
			$this->db->where ( 'type', 'Group' );
		}
		$res = $this->db->get ( $this->post_like_table );
		$rows = $res->num_rows ();
		
		if ($rows == 1) {
			
			return true;
		}
	}
}

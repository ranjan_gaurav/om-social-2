<?php

class Wall_model extends CI_Model {

    var $table = 'users_wall';
    var $table_poll_options = 'poll_options';
    var $table_poll_votes = 'poll_votes';
    var $user_table = 'users';
    var $Likes = 'activity_likes';
    var $Comments = 'activity_comments';

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    Public function getAllpolls() {
        $this->db->select('*');
        $this->db->from($this->table_poll);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function getAllpollOptions() {
        $this->db->select('*');
        $this->db->from($this->table_poll_options);
        $res = $this->db->get();
        $result = array();
        $ids = array();
        foreach ($res->result_array() as $list) {
            $poll_id = $list['poll_id'];
            if (!in_array($poll_id, $ids)) {
                array_push($ids, $poll_id);
                $result[$poll_id][] = $list;
            } else {
                array_push($result[$poll_id], $list);
            }
        }
        return $result;
    }

    public function addWallPost($data,$sendername) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'senderuserid', 'label' => 'senderuserid', 'rules' => 'trim|required'),
                array('field' => 'receiveruserid', 'label' => 'receiveruserid', 'rules' => 'trim|required'),
            	array('field' => 'body', 'label' => 'body', 'rules' => 'trim|required'),
               
            );

            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
            	$this->db->set ( 'created_date', 'NOW()', FALSE );
            	$this->db->insert ( $this->table, $data );
            	//echo $this->db->last_query(); die();
            	$insert_id = $this->db->insert_id ();
            	if ($insert_id) {
            		
            	$res = $this->GetUsersData($data);
            	
            	//print_r($res);die();
            	$GetDeviceType = $res->device_name;
			    $posterid =  $data['senderuser_id'];
			    
			    $Details = $this->getUpdatedWall($insert_id);
			    
			    $msg = $sendername . " has posted on your wall";
		        $Type = "WallPost";
			    
			    if($GetDeviceType == 'android')
			    {
			    	$deviceID = $res->reg_id;
			    	$this->Android_notification( $posterid, $msg, $Type, $deviceID, $Details );
			    }
			    
			    else {
			    	$deviceID = $res->reg_id_ios;
			    	$this->Ios_notification ( $posterid, $msg, $Type, $deviceID, $Details );
			    }
            	
            	$message = array (
            			'status' => true,
            			'response_code' => '1',
            			'message' => "You have successfully posted on the ".$res->user_name.''." wall",
            			
            	);
               
            	}
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function checkUserPoll($poll_id, $user_id) {
        $conditions = array(
            'user_unique_id' => $user_id,
            'poll_id' => $poll_id
        );
        $this->db->where($conditions);
        $res = $this->db->get($this->table_poll_votes);
        if ($res->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function GetUsersData($data) {
    	
       $this->db->where('id',$this->input->post('receiveruserid'));
       $res = $this->db->get($this->user_table);
      // echo $this->db->last_query();die();
       
       return $res->row();
    }
    
    public function GetWallPostsById($userid,$walluserid)
    {
    	$response = array ();
    	$sql = ('
SELECT `users_wall`.*,users.user_name as Sendername,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) user_pic,
count(distinct(activity_likes.like_id)) as likes_count ,count(distinct(activity_comments.comment_id)) as comments_count
FROM `users_wall`
left JOIN `users` ON `users_wall`.`senderuser_id` =  `users`.`id`
left JOIN `activity_likes` ON `users_wall`.`wall_id` =  `activity_likes`.`poster_id` and activity_likes.type = "wall"
left JOIN `activity_comments` ON `users_wall`.`wall_id` =  `activity_comments`.`poster_id` and activity_comments.type = "wall"
WHERE `receiveruser_id` = '. $walluserid .'
Group BY users_wall.wall_id

    			
UNION

SELECT `users_wall`.*,users.user_name as Sendername,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) user_pic,
count(distinct(activity_likes.like_id)) as likes_count  ,count(distinct(activity_comments.comment_id)) as comments_count
FROM `users_wall`
left JOIN `users` ON `users_wall`.`senderuser_id` =  `users`.`id`
left JOIN `activity_likes` ON `users_wall`.`wall_id` =  `activity_likes`.`poster_id` and activity_likes.type = "wall"
left JOIN `activity_comments` ON `users_wall`.`wall_id` =  `activity_comments`.`poster_id` and activity_comments.type = "wall"
WHERE `senderuser_id` = '. $walluserid .'
Group BY users_wall.wall_id

    			
 ');
    	$query = $this->db->query($sql);
  //  echo $this->db->last_query(); die();
    	$result =  $query->result_array();
    	
    	foreach ( $result as $row ) {
    		// print_r($row);
    		$postID = $row ['wall_id'];
    		$senderID = $row['senderuser_id'];
    		// $row["comments"] = (object) array();
    		$val = $this->getLikeStatus ( $postID, $userid );
    			
    		if($val)
    		{
    			$row["is_liked"] = '1';
    		}
    			
    		else
    		{
    			$row["is_liked"] = '0';
    		}
    		
    		$row ["comments"] =  ( object ) ( array )$this->getAllComments ($postID);
    		$row ["Sender"] =  ( object ) ( array )$this->getsender ($senderID);
    			
    		array_push ( $response, $row );
    		// $i++;
    		// print_r($response);
    	}
    	
    	return $response;
    	
    }
    
    
    public function getAllComments($postid) {
    	$this->db->limit ( 4 );
    	$this->db->order_by ( "created_date", "asc" );
    	//$this->db->where ( 'group_id_fk', $groupid );
    	$this->db->where ( 'users_wall.wall_id', $postid );
    	$this->db->select ( 'users_wall.wall_id , activity_comments.comment_id,activity_comments.body,users.id,users.user_name , IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
    	// $this->db->select ('activity_comments.body' );
    	$this->db->from ( $this->table);
    	$this->db->join ( $this->Comments, 'users_wall.wall_id =activity_comments.poster_id and activity_comments.type = "wall"');
    	$this->db->join ( $this->user_table, 'activity_comments.user_id =users.id', 'left' );
    	$res = $this->db->get ();
    	// echo $this->db->last_query(); die();
    	return $res->result();
    }
    
    
    public function getsender ($senderID)
    {
    	$this->db->where( 'id', $senderID);
    	
    	$this->db->select ( 'users.*,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
    	$this->db->from ( $this->user_table );
    
    	$res = $this->db->get ();
    	// echo $this->db->last_query(); die();
    	return $res->row();
    }
    
    public function EditWallPost($wall_id,$data)
    {   
    	$this->db->set ( 'modified_date', 'NOW()', FALSE );
    	$this->db->where('wall_id',$wall_id);
    	$query = $this->db->update ( $this->table, $data );
    	if($query)
    	{
    		$res = $this->getUpdatedWall($wall_id);
    		
    		if($res)
    		{
    			$message = array (
    					'status' => true,
    					'response_code' => '1',
    					'message' => "Wall Successfully Updated",
    					'updated_wall' => $res->body
    			);
    		}
    		
    		else {
    			$message = [
    					'response_code' => '0',
    					'message' => 'Something went wrong'
    			];
    		}
    	}
    	
    	else {
    		$message = [
    				'response_code' => '0',
    				'message' => 'Something went wrong'
    		];
    	}
    	
    	return $message;
    }
    
    public function getUpdatedWall($wall_id)
    {
    	$this->db->where ( 'wall_id', $wall_id );
    	$res = $this->db->get ( $this->table);
    	// echo $this->db->last_query(); die();
    	return $res->row();
    }
    
    public function DeleteWallPost($wall_id)
    {
    	$this->db->where ( 'wall_id', $wall_id );
    	$query = $this->db->delete ( $this->table );
    	if ($query) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    public function getLikeStatus ( $posterID, $userid )
    {
    	$this->db->where ( 'poster_id', $posterID );
    	$this->db->where ( 'user_id', $userid );
    	$this->db->where ('type','wall');
    	$res = $this->db->get ( $this->Likes);
    	$rows = $res->num_rows ();
    
    	if ($rows == 1) {
    
    		return true;
    	}
    
    
    }
    
    
    public function Android_notification( $posterid, $msg, $Type, $deviceID, $Details )
    {
    	$get = json_decode ( $deviceID );
    	
    	// print_r($deviceID); die();
    	
    	/*
    	 * $data['data'] = array(
    	 * 'type' => 'Poke',
    	 *
    	 * );
    	 */
    	// print_r($get);
    	
    	// $notification = GetNonActionNotificationDetails($action);
    	// $message = 'Hi i am sending push notification';
    	$api_key = 'AIzaSyDCVGdZc9Abs5cRWPCMWWiKejz9W3hX-wE';
    	
    	if (empty ( $api_key ) || count ( $get ) < 0) {
    		$result = array (
    				'success' => '0',
    				'message' => 'api or reg id not found'
    		);
    		echo json_encode ( $result );
    		die ();
    	}
    	$registrationIDs = $get;
    	
    	$message = $msg;
    	$url = 'https://android.googleapis.com/gcm/send';
    	$resultData = array (
    			"type" => $Type,
    			"title" => 'Notification of wall',
    			"message" => $message,
    			"poster_id" => $posterid,
    			"wall" => $Details
    	)
    	;
    	
    	 //print_r($resultData); die();
    	
    	$fields = array (
    			'registration_ids' => $registrationIDs,
    			'data' => array (
    					"message" => $message,
    					"title" => 'Notification of Group',
    					"type" => $Type,
    					'data' => $resultData
    			)
    	);
    	// print_r($fields); //die();
    	$headers = array (
    			'Authorization: key=' . $api_key,
    			'Content-Type: application/json'
    	);
    	
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_POST, true );
    	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    	curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
    	$result = curl_exec ( $ch );
    	
    	curl_close ( $ch );
    	// print_r ( $result );
    }
    
    public function Ios_notification ( $posterid, $msg, $Type, $deviceID, $Details )
    {
    	//print_r($Details); die();
    	
    	set_time_limit ( 0 );
    	
    	// charset header for output
    	header ( 'content-type: text/html; charset: utf-8' );
    	
    	// this is the pass phrase you defined when creating the key
    	$passphrase = '123456';
    	
    	// you can post a variable to this string or edit the message here
    	if (! isset ( $_POST ['msg'] )) {
    		$_POST ['msg'] = $msg;
    	}
    	
    	// tr_to_utf function needed to fix the Turkish characters
    	$message = $_POST ['msg'];
    	
    	
    	//print_r($message); die();
    	
    	// load your device ids to an array
    	
    	/*
    	 * $item = array (
    	 * 'f9c68652e479664827af1c9892e04c2c6b8398561f7e34e3d9f0125f58fc94f1'
    	 * );
    	 */
    	
    	// this is where you can customize your notification
    	$payload = '{"aps":{
		"alert":"' . $message . '",
		"sound":"default",
		"type":"'.$Type.'",
		"posterid":"'.$posterid.'",
		"wall":"'.$Details->wall_id.'"
}}';
    	
    	$result = 'Start' . '<br />';
    	
    	// //////////////////////////////////////////////////////////////////////////////
    	// start to create connection
    	$ctx = stream_context_create ();
    	stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'apnsdev.pem' );
    	stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
    	
    	// echo count ( $deviceIds ) . ' devices will receive notifications.<br />';
    	
    	$get = json_decode ( $deviceID );
    	
    	// print_r($get); exit;
    	
    	foreach ( $get as $val ) {
    	
    		// print_r($val);
    		// wait for some time
    		sleep ( 1 );
    	
    		// Open a connection to the APNS server
    		$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
    	
    		if (! $fp) {
    			// exit ( "Failed to connect: $err $errstr" . '<br />' );
    		} else {
    			//echo 'Apple service is online. ' . '<br />';
    		}
    	
    		/*
    		 * $payload['aps'] = array(
    		 * 'alert' => $message,
    		 * 'badge' => $badge,
    		 * 'sound' => 'default'
    		 * );
    		 * $body['data'] = $data;
    		 */
    	
    		// Encode the payload as JSON
    		// $payload = json_encode($body);
    	
    		// Build the binary notification
    		$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $val ) . pack ( 'n', strlen ( $payload ) ) . $payload;
    	
    		// Send it to the server
    		$result = fwrite ( $fp, $msg, strlen ( $msg ) );
    	
    		if (! $result) {
    			// echo 'Undelivered message count: ' . $item . '<br />';
    		} else {
    			//echo $payload;
    		}
    	
    		if ($fp) {
    			fclose ( $fp );
    			// echo 'The connection has been closed by the client' . '<br />';
    		}
    	}
    	
    	// echo count($deviceIds) . ' devices have received notifications.<br />';
    	
    	// function for fixing Turkish characters
    	/*
    	 * function tr_to_utf($text) {
    	 * $text = trim ( $text );
    	 * $search = array (
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�',
    	 * '�'
    	 * );
    	 * $replace = array (
    	 * 'Ü',
    	 * 'Ş',
    	 * '&#286;�',
    	 * 'Ç',
    	 * 'İ',
    	 * 'Ö',
    	 * 'ü',
    	 * 'ş',
    	 * 'ğ',
    	 * 'ç',
    	 * 'ı',
    	 * 'ö'
    	 * );
    	 * $new_text = str_replace ( $search, $replace, $text );
    	 * return $new_text;
    	 * }
    	 */
    	
    	// set time limit back to a normal value
    	set_time_limit ( 30 );
    	}
    

}

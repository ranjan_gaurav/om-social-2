<?php

class Comments_model extends CI_Model {

    var $comment_table = "event_comments";
    var $activity_table = "activity_comments";
    var $user_table = "users";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    public function insertComments($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'poster_id', 'label' => 'UserID', 'rules' => 'trim|required'),
                array('field' => 'resource_id', 'label' => 'Resource ID', 'rules' => 'trim|required'),
                array('field' => 'type', 'label' => 'Type', 'Please Define type' => 'trim|required'),
                array('field' => 'body', 'label' => 'Comment', 'Please enter comment' => 'trim|required'),
            );

            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $data_comments = array('poster_id' => $data['poster_id'],
                    'resource_id' => $data['resource_id'],
                    'body' => $data['body'],
                    'creation_date' => date('Y-m-d H:i:s'));
                $data_activity = array('poster_id' => $data['poster_id'],
                    'resource_id' => $data['resource_id'],
                    'body' => $data['body'],
                    'creation_date' => date('Y-m-d H:i:s')
                );
                if ($data['type'] == 'event') {
                    $this->db->insert('event_comments', $data_comments);
                    $insertId=$this->db->insert_id();
                }
                if ($data['type'] == 'post') {
                    $this->db->insert('activity_comments', $data_activity);
                    $insertId=$this->db->insert_id();
                }
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'comment_id'=>$insertId
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }

        return $message;
    }

    public function getAllComments($event_id, $list_type) {

        if ($list_type == trim('events')) {
            $this->db->select('comm.comment_id,comm.resource_id as event_id,comm.body,us.id as user_id,if(us.user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '/",us.user_pic ) ) user_pic,us.user_name,creation_date,');
            $this->db->where('resource_id', $event_id);
            $this->db->order_by('creation_date', 'DESC');
            $this->db->from($this->comment_table . ' comm');
            $this->db->join($this->user_table . ' us', '`us`.`id`=`comm`.`poster_id`');
            $res = $this->db->get();
            return $res->result_array();
        }
        if ($list_type == trim('activity')) {
            $this->db->select('comm.comment_id,comm.resource_id as event_id,comm.body,us.id as user_id,if(us.user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '/",us.user_pic ) ) user_pic,us.user_name,creation_date,');
            $this->db->where('resource_id', $event_id);
            $this->db->order_by('creation_date', 'DESC');
            $this->db->from($this->activity_table . ' comm');
            $this->db->join($this->user_table . ' us', '`us`.`id`=`comm`.`poster_id`');
            $res = $this->db->get();
            return $res->result_array();
        }
    }

    public function delete($id) {
        if (isset($id) && $id != '') {
            $this->db->where('comment_id', $id);
            $query = $this->db->delete($this->activity_table);
            if ($query) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public function getActivity($id){
        $this->db->select('resource_id');
        $this->db->where('comment_id', $id);
        $this->db->from($this->activity_table);
        $res=$this->db->get();
        return $res->row();
    }
    
    public function updateComment($id){
        $comment_count=$this->db->select('comment_count')->where('activity_id', $id)->get('activities')->row()->comment_count;
        $data=array(
            'comment_count'=>$comment_count-1,
        );
        $this->db->where('activity_id', $id);
        $this->db->update('activities',$data);
    }

}

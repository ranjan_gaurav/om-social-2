<?php

class Poll_model extends CI_Model {

    var $table_poll = 'polls';
    var $table_poll_options = 'poll_options';
    var $table_poll_votes = 'poll_votes';
    var $user_table = 'users';

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    Public function getAllpolls() {
        $this->db->select('*');
        $this->db->from($this->table_poll);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function getAllpollOptions() {
        $this->db->select('*');
        $this->db->from($this->table_poll_options);
        $res = $this->db->get();
        $result = array();
        $ids = array();
        foreach ($res->result_array() as $list) {
            $poll_id = $list['poll_id'];
            if (!in_array($poll_id, $ids)) {
                array_push($ids, $poll_id);
                $result[$poll_id][] = $list;
            } else {
                array_push($result[$poll_id], $list);
            }
        }
        return $result;
    }

    public function setpollVote($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'poll_id', 'label' => 'Poll ID', 'rules' => 'trim|required'),
                array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required'),
                array('field' => 'poll_option_id', 'label' => 'Poll Option ID', 'rules' => 'trim|required'),
                array('field' => 'poll_vote_count', 'label' => 'Votes Count For Poll', 'rules' => 'trim|required'),
            );

            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $countVote = $this->checkUserPoll($data['poll_id'], $data['user_id']);
                if (!$countVote) {
                    $ins = array();
                    $ins['poll_id'] = $data['poll_id'];
                    $ins['user_unique_id'] = $data['user_id'];
                    $ins['poll_option_id'] = $data['poll_option_id'];
                    $ins['creation_date'] = date("Y-m-d H:i:s");
                    $this->db->insert($this->table_poll_votes, $ins);
                    $options['vote_count'] = PollTotalVotes($data['poll_id']) + 1;
                    $this->db->where('poll_id', $data['poll_id']);
                    $this->db->update($this->table_poll, $options);
                    $votesOption['votes'] = OptionTotalVotes($data['poll_option_id']) + 1;
                    $this->db->where('poll_option_id', $data['poll_option_id']);
                    $this->db->update($this->table_poll_options, $votesOption);
                    $message = array(
                        'status' => true,
                        'response_code' => '1'
                    );
                } else {
                    $message = array(
                        'status' => true,
                        'response_code' => '1'
                    );
                }
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function checkUserPoll($poll_id, $user_id) {
        $conditions = array(
            'user_unique_id' => $user_id,
            'poll_id' => $poll_id
        );
        $this->db->where($conditions);
        $res = $this->db->get($this->table_poll_votes);
        if ($res->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function totalUsers() {
        $res = $this->db->get($this->user_table);
        return $res->num_rows();
    }

}

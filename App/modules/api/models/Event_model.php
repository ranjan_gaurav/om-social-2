<?php

class Event_model extends CI_Model {

    var $event_table = "events";
    var $category_table = "event_categories";
    var $comment_table = "event_comments";
    var $user_table = "users";
    var $attendees = "event_membership";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    Public function getAllCategories() {
        $this->db->select('*');
        $this->db->from($this->category_table);
        $res = $this->db->get();
        return $res->result_array();
    }

    Public function getAllEvents($cat_id, $type) {
        date_default_timezone_set('Asia/Kolkata');
        $todayDate = date("Y-m-d");
        if ($type == 'upcoming') {
            $this->db->where('eve.event_date >=', $todayDate);
        } elseif ($type == 'past') {
            $this->db->where('eve.event_date <', $todayDate);
        }
        if ($cat_id != '0') {
            $this->db->where('eve.cat_id', $cat_id);
        }
        $this->db->order_by('event_date', 'ASC');
        $this->db->select('eve.event_id,eve.cat_id,eve.title,eve.description,eve.creation_date,eve.modification_date,eve.approval,eve.like_count,eve.comment_count,eve.event_date,eve.starttime,eve.endtime,eve.location,if(!eve.photo_id,"null" ,CONCAT("' . base_url('uploads/events/cover/') . '/",eve.photo_id ) ) photo_id,cat.cat_name');
        $this->db->from($this->event_table . ' eve');
        $this->db->join($this->category_table . ' cat', '`cat`.`cat_id`=`eve`.`cat_id`');
        $res = $this->db->get();
        return $res->result_array();
    }

    Public function getEvent($ID = null) {
        if ($ID != null) {
            $this->db->where('eve.event_id', $ID);
        }
        $this->db->select('eve.event_id,eve.cat_id,eve.title,eve.description,eve.creation_date,eve.modification_date,eve.approval,eve.like_count,eve.comment_count,eve.event_date,eve.starttime,eve.endtime,eve.location,if(!eve.photo_id,"null" ,CONCAT("' . base_url('uploads/events/cover/') . '/",eve.photo_id ) ) photo_id,cat.cat_name');
        $this->db->from($this->event_table . ' eve');
        $this->db->join($this->category_table . ' cat', '`cat`.`cat_id`=`eve`.`cat_id`');
        $res = $this->db->get();
        return $res->result_array();
    }

    Public function getAllEventsPhotos($id, $base_photo_url) {
        $this->db->select('events.event_id,if(!event_photos.photo_name,"null" ,CONCAT("' . $base_photo_url . '",event_photos.photo_name ) ) photo_name');
        $this->db->from('events');
        $this->db->join('event_photos', 'event_photos.event_id = events.event_id');
        $this->db->where('events.event_id', $id);
        $res = $this->db->get();
        return $res->result_array();
    }

    public function getCountComment($id) {
        $this->db->select('comment_count');
        $this->db->from('events');
        $this->db->where('event_id', $id);
        $res = $this->db->get();
        return $res->row();
    }

    public function getCountActivity($id) {
        $this->db->select('comment_count');
        $this->db->from('activities');
        $this->db->where('activity_id', $id);
        $res = $this->db->get();
        return $res->row();
    }

    public function updateCounter($value, $event_id, $event_type) {
        $data = array('comment_count' => $value);
        if ($event_type == 'event') {
            $this->db->where('event_id', $event_id);
            $this->db->update('events', $data);
        }
        if ($event_type == 'post') {
            $this->db->where('activity_id', $event_id);
            $this->db->update('activities', $data);
        }
    }

    public function getComments($id) {
        $this->db->select('comm.comment_id,comm.resource_id as event_id,comm.body,creation_date,us.id as user_id,if(us.user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '",us.user_pic ) ) user_pic,us.user_name');
        $this->db->where('comm.resource_id', $id);
        $this->db->limit(3);
        $this->db->from($this->comment_table . ' comm');
        $this->db->order_by('creation_date', 'DESC');
        $this->db->join($this->user_table . ' us', '`us`.`id`=`comm`.`poster_id`');
        $res = $this->db->get();
        return $res->result_array();
    }

    Public function getAllCalenderEvents() {
        $this->db->order_by('event_date', 'ASC');
        $this->db->select('eve.event_id,eve.title,eve.description,eve.creation_date,eve.modification_date,eve.host,eve.approval,eve.like_count,eve.comment_count,eve.event_date,DATE_FORMAT(eve.starttime, "%h:%i %p") as starttime,DATE_FORMAT(eve.endtime, "%h:%i %p") as endtime,eve.location,if(!eve.photo_id,"null" ,CONCAT("' . base_url('uploads/events/cover/') . '/",eve.photo_id ) ) photo_id');
        $this->db->from($this->event_table . ' eve');
        $res = $this->db->get();
        $result = array();
        $dateArray = array();
        $j = 0;
        $k = 0;
        $eventDate = '';
        foreach ($res->result_array() as $list) {
            if ($eventDate == $list['event_date']) {
                $result[$j]['date'] = $list['event_date'];
                $result[$j]['data'][] = $list;
            } else {
                if ($k != 0):
                    $j++;
                endif;
                $result[$j]['date'] = $list['event_date'];
                $result[$j]['data'][] = $list;
            }
            $eventDate = $list['event_date'];
            $k++;
            //print_r($list);
        }
        return $result;
    }

    public function checkDuplicateAttendees($data) {
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('resource_id', $data['resource_id']);
        $query = $this->db->get('event_membership');
        $count_row = $query->num_rows();
        if ($count_row > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function insertAttendees($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_id', 'label' => 'UserID', 'rules' => 'trim|required'),
                array('field' => 'resource_id', 'label' => 'ResourceID', 'rules' => 'trim|required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            }else {
                $this->db->where('user_id', $data['user_id']);
                $this->db->where('resource_id', $data['resource_id']);
                $query = $this->db->get('event_membership');
                $count_row = $query->num_rows();
                if ($count_row > 0) {
                    $message = array(
                        'status' => False,
                        'response_code' => '0',
                        'message' => 'Duplicate Record'
                    );
                } else {
                    $data_attendees = array('user_id' => $data['user_id'],
                        'resource_id' => $data['resource_id']);
                    $this->db->insert('event_membership', $data_attendees);
                    $message = array(
                        'status' => true,
                        'response_code' => '1'
                    );
                }
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function listAttendees() {
        $this->db->select('id,userid,user_address,cohort,user_email,if(user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '/",user_pic ) ) user_pic,user_name');
        $this->db->where('is_reset','1');
        $this->db->order_by('user_name','ASC');
        $this->db->from('users us');
        //$this->db->join('event_membership eve', '`eve`.`user_id`=`us`.`id`');
        //$this->db->group_by('`eve`.`user_id`');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function listLeadershipAttendees() {
        $this->db->select('userid , user_email,if(user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '/",user_pic ) ) user_pic,
            (((SELECT count(`like_id`) FROM `event_likes` WHERE `poster_id` =`users`.id ))+
            ((SELECT count(`like_id`) FROM `activity_likes` WHERE `poster_id` =`users`.id ) )+
            ((SELECT count(comment_id) FROM `activity_comments` WHERE  poster_id =`users`.id ) )+
            ((SELECT COUNT(comment_id) FROM `event_comments` WHERE `poster_id` =`users`.id ) )+
            (SELECT count(activity_id)+IFNULL(SUM(`like_count`),0)+ IFNULL(SUM(`comment_count`),0) FROM `activities` WHERE  activities.user_id= `users`.id))  total_score
        ');
        $ignore=array(4);
        $this->db->where_not_in('id',$ignore);
        $this->db->having('total_score >', '0');
        $this->db->order_by('total_score', 'DESC');
        $this->db->having('total_score >', '0');
        $this->db->from('users');
        $res = $this->db->get();
        
        return $res->result_array();
    }

    public function listAttendeesTest() {
        $this->db->select('userid , user_email,if(user_pic="","null" ,CONCAT("' . base_url('uploads/users/profile/') . '",user_pic ) ) user_pic,
            (SELECT count(`like_id`) FROM `event_likes` WHERE `poster_id` =`users`.id ) as like_count,
            (SELECT count(`like_id`) FROM `activity_likes` WHERE `poster_id` =`users`.id ) as activity_count,
            (SELECT count(comment_id) FROM `activity_comments` WHERE  poster_id =`users`.id ) as activity_comment,
            (SELECT COUNT(comment_id) FROM `event_comments` WHERE `poster_id` =`users`.id ) as events_comment,
            (SELECT count(activity_id)+IFNULL(SUM(`like_count`),0)+ IFNULL(SUM(`comment_count`),0) FROM `activities` WHERE  activities.user_id= `users`.id) as activity_score,
            (((SELECT count(`like_id`) FROM `event_likes` WHERE `poster_id` =`users`.id ))+
            ((SELECT count(`like_id`) FROM `activity_likes` WHERE `poster_id` =`users`.id ) )+
            ((SELECT count(comment_id) FROM `activity_comments` WHERE  poster_id =`users`.id ) )+
            ((SELECT COUNT(comment_id) FROM `event_comments` WHERE `poster_id` =`users`.id ) )+
            (SELECT count(activity_id)+IFNULL(SUM(`like_count`),0)+ IFNULL(SUM(`comment_count`),0) FROM `activities` WHERE  activities.user_id= `users`.id))  total_score
        ');
        $this->db->from('users');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function insertNewAttendees($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_id', 'label' => 'UserID', 'rules' => 'trim|required'),
                array('field' => 'resource_id', 'label' => 'ResourceID', 'rules' => 'trim|required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            }else {
                $updateMembership = array(
                    'resource_id' => $data['resource_id'],
                    'user_id' => $data['user_id'],
                );
                $query = $this->db->insert('event_membership', $updateMembership);
                if ($query) {
                    $message = array(
                        'status' => true,
                        'response_code' => '1',
                        'message' => 'Thanks for attending'
                    );
                } else {
                    $message = array(
                        'status' => false,
                        'response_code' => '0'
                    );
                }
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function GetEventsBytime() {
        date_default_timezone_set('Asia/Calcutta');
        $startTime = date('H:i:s');
        $endtime = date("H:i:s", strtotime('+15 minutes'));
        $date = date("Y-m-d");
        $this->db->where('event_date =', $date);
        $this->db->where('starttime >=', $startTime);
        $this->db->where('starttime <=', $endtime);
        $this->db->from('events');
        $this->db->limit(1);
        $res = $this->db->get();
        return $res->row();
    }
    
    public function MakeReadAttendeesNotification($data){
        $conditions=array(
            'resource_id'=>$data['resource_id'],
            'user_id'=>$data['user_id'],
            'type'=>'attendance',
        );
        $this->db->where($conditions);
        $update=array(
            'status'=>'1'
        );
        $this->db->update('notifications',$update);
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 03/22/2017
 * Time: 15:28 AM
 */
class User_model extends CI_Model {

    var $user_table = "users";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @param null $ID
     * @return User data

     */
    public function getAllUsers($ID = null) {
        if ($ID != null) {
            $this->db->where('id !=', $ID);
        }
        $this->db->where('status', '1');
        $this->db->from($this->user_table);
        $result = $this->db->get();
        return $result->result();
    }

    public function get_details_result($user_id) {
        $this->db->where('id', $user_id);
        $this->db->select('id,user_pic');
        $this->db->from($this->user_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * @method : Add User
     * @param $data
     * @return bool
     * @date : 03/22/2017
     * @description : Add new User in the User table
     */
    public function create($data) {
        try {
            $this->load->library('form_validation');
          
                $config = array(
                    array('field' => 'user_id', 'label' => 'UserID', 'rules' => 'trim|required|is_unique[users.user_id]'),
                	array('field' => 'user_email', 'label' => 'User-Email', 'rules' => 'trim|required|valid_email|is_unique[users.user_email]'),
                    array('field' => 'user_password', 'label' => 'User-Password', 'rules' => 'trim|required'),
                    array('field' => 'user_mobile', 'label' => 'User-Mobile', 'rules' => 'trim|required|numeric')
                );
            
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('registered_date', 'NOW()', false);
                $this->db->insert($this->user_table, $data);
                $this->load->library('email');
                $this->email->from('info@orangemantra.com', 'Om Social');
                $this->email->to($data['user_email']);
                $this->email->subject('New Registration');
                $this->email->message('Thank you for registering with Om Social!');
                $this->email->send();
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "User Added Successfully"
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @method : Update User
     * @description : To update User
     */
    public function update($id, $data) {
        try {
            unset($data['id']);
            $this->load->library('form_validation');
            $update_data = $data;
            $file = $this->upload_profile_pic();
            if ($file['file_name'] != '') {
                $update_data = array(
                    'user_pic' => $file['file_name']
                );
            }
            $this->db->set('user_modification_date', 'NOW()', FALSE);
            $this->db->where('id', $id);
            $query = $this->db->update($this->user_table, $update_data);
            if ($query) {
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "User Successfully Updated"
                );
            } else {
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'Something went wrong , Please try again.'
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @method :  Upload Profile Pic
     * @description : To Upload Profile Pic
     */
    protected function upload_profile_pic() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        $config['upload_path'] = IMAGESPATH . 'users/profile/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('user_pic')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    /**
     * @method :  Delete User
     * @description : To delete User from User table
     */
    public function delete($id) {
        if (isset($id) && $id != '') {
            $this->db->where('id', $id);
            $query = $this->db->delete($this->user_table);
            if ($query) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    
    /*
     * Author : Gaurav Ranjan
     * 
     * Date   : 22-03-2017
     * 
     * Description : Validate Userdata
     */

    function validateUserdata($userid, $password) {
        $condition = array(
            'user_email' => $userid,
            'user_password' => $password,
        );
        $this->db->where($condition);
        $this->db->select('id,user_id,user_name,user_email,user_mobile,user_dob,user_gender,user_address,user_pic,status,is_reset,fcm_token_id');
        $query = $this->db->get($this->user_table);
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    function valid_current_pass($user_id, $current_pass) {
        $cond = array(
            'userid' => $user_id,
            'user_pass' => $current_pass,
        );
        $this->db->where($cond);
        $query = $this->db->get($this->user_table);
        
       // echo $this->db->last_query(); die();
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    // reset new password
    function updatepassword($table, $user_id, $new_pass, $reset = '') {
        $user_email = $this->db
                        ->select('user_email')
                        ->where('userid', $user_id)
                        ->limit(1)
                        ->get('users')
                        ->result_array()[0]['user_email'];
        $useremail = $user_email;
        $this->load->library('email');
        $this->email->from('info@orangemantra.com', 'Orange Mantra');
        $this->email->to($useremail);
        $this->email->subject('change Password');
        $this->email->message('Hi Your password has been changed: New Password is: ' . $new_pass);
        $this->email->send();
        $post = $this->input->post();
        $ins = array();
        $ins['user_pass'] = md5($new_pass);
        if ($reset != ''):
            $ins['is_reset'] = '1';
        endif;
        $this->db->where('userid', $user_id);
        $this->db->update($table, $ins);
    }

    
    
    
    public function validate_password($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_email', 'label' => 'User Email', 'rules' => 'trim|required|valid_email')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $randcode = random_string('numeric', 16);
                $random_code = substr($randcode, 0, 5);
                $pass = $random_code; //$this->generateStrongPassword('9');
                $this->load->library('email');
                $this->email->from('info@orangemantra.com', 'Orange Mantra');
                $this->email->to($data['user_email']);
                $this->email->subject('Forgot Password');
                $this->email->message('Hi Your password has been changed: New Password is: ' . $pass);
                $this->email->send();
                $user_email = $data['user_email'];
                $updtate = array(
                    'user_password' => md5($pass)
                );
                $cond = array(
                    'user_email' => $user_email,
                );
                $this->db->where($cond);
                $query = $this->db->update($this->user_table, $updtate);
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'password' => $pass,
                    'message' => 'Password has been changed succeessfully.Please Check you mail.Thank You!'
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function updateprofile($id) {
        $file = $this->upload_profile_pic();
        if ($file['file_name'] != '') {
            $data = array(
                'user_pic' => $file['file_name']
            );
            $this->db->where('id', $id);
            $query = $this->db->update($this->user_table, $data);
            if ($query) {
                $filepath = base_url() . 'uploads/users/profile/' . $file['file_name'];
                return $filepath;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /*
     * Author :  Gaurav Ranjan
     * 
     * Date   : 22-03-2017
     * 
     * Description : To check user existence
     * 
     */
    

    public function checkUser($data) {
        $userid = $data['user_id'];
        $this->db->where('user_id', $userid);
        $res = $this->db->get($this->user_table);
        if ($res->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function CheckUserCredentials($data) {
        $cond = array(
            'user_email' => $data['user_email'],
        );
        $this->db->where($cond);
        $res = $this->db->get($this->user_table);
        if ($res->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // reg_id chek  for ios
    function checkreg_id_ios($table, $user_id) {
        $this->db->select('reg_id_ios');
        $this->db->where('user_email', $user_id);
        $query = $this->db->get($table);
        //echo $this->db->last_query();die;
        return $query->row();
    }

    // update reg_id for ios
    function updateregid_ios($table, $user_id, $reg_id_ios,$type) {
        $data = array();
        $data['reg_id_ios'] = json_encode($reg_id_ios);
        $data['device_name'] = $type;

        $this->db->where('user_email', $user_id);
        $this->db->update($table, $data);
    }

    // reg_id chek  android
    function checkreg_id($table, $user_id) {
        $this->db->select('reg_id');
        $this->db->where('user_email', $user_id);
        $query = $this->db->get($table);
        return $query->row();
    }

    // update reg_id for android
    function updateregid($table, $user_id, $reg_id,$type) {
        $data = array();
        $data['reg_id'] = json_encode($reg_id);
        $data['device_name'] = $type;

        $this->db->where('user_email', $user_id);
        $this->db->update($table, $data);
    }

    // update reg_id ios
    function updaterlogout_ios($table, $user_id, $data,$fcm) {

        
        $this->db->where('id', $user_id);
        $this->db->update($table, $data);
       // echo $this->db->last_query(); die();
    }

    // update reg_id android
    function updaterlogout($table, $user_id, $data,$fcm) {
    	
        $this->db->where('id', $user_id);
        $this->db->update($table, $data);
        //echo $this->db->last_query(); //die();
       
    }
    
    function UpdateFCM($table,$user_id,$fcm)
    {
    	$data['fcm_token_id'] = $fcm;
    	$this->db->where('user_email', $user_id);
    	$this->db->update($table, $data);
    	//echo $this->db->last_query(); die();
    }

   
    

}

?>
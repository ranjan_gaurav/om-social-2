<?php

class Exhibitor_model extends CI_Model {

    var $event_table = "events";
    var $exhibitor_table = "event_exhibitors";
    var $user_table = "users";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    Public function getAllexhibitors($event_id = null, $exhibitor_id = null) {
        if ($event_id != '0' && $event_id != null) {
            $this->db->where('event_id', $event_id);
        }
        if ($exhibitor_id != '0' && $exhibitor_id != null) {
            $this->db->where('exhibitor_id', $exhibitor_id);
        }
        $this->db->select('*,if(photo_id="","null" ,CONCAT("' . base_url('uploads/exhibitors/') . '/",photo_id ) ) photo_id');
        $this->db->from($this->exhibitor_table);
        $res = $this->db->get();
        return $res->result_array();
    }

}

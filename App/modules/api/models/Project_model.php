<?php
class Project_model extends CI_Model {
	var $table = 'projects';
	var $group = 'groups';
	var $table_project = 'project_members';
	var $table_post = 'project_post';
	var $user_table = 'users';
	var $Likes = 'activity_likes';
	var $Comments = 'activity_comments';
	function __construct() {
		parent::__construct ();
		date_default_timezone_set ( 'Asia/Kolkata' );
	}
	Public function getAllpolls() {
		$this->db->select ( '*' );
		$this->db->from ( $this->table_poll );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	public function getAllpollOptions() {
		$this->db->select ( '*' );
		$this->db->from ( $this->table_poll_options );
		$res = $this->db->get ();
		$result = array ();
		$ids = array ();
		foreach ( $res->result_array () as $list ) {
			$poll_id = $list ['poll_id'];
			if (! in_array ( $poll_id, $ids )) {
				array_push ( $ids, $poll_id );
				$result [$poll_id] [] = $list;
			} else {
				array_push ( $result [$poll_id], $list );
			}
		}
		return $result;
	}
	public function CreateProjects($data, $json,$userid,$Sendername) {
		// error_reporting ( 0 );
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'project_title',
							'label' => 'project_title',
							'rules' => 'trim|required|is_unique[projects.project_title]' 
					),
					array (
							'field' => 'project_admin',
							'label' => 'project_admin',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			$this->form_validation->set_message ( 'is_unique', 'The %s is already taken' );
			
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				// $countVote = $this->checkUserPoll($data['poll_id'], $data['user_id']);
				
				$this->db->set ( 'created_date', 'NOW()', FALSE );
				$this->db->insert ( $this->table, $data );
				$projectid = $this->db->insert_id ();
				if ($projectid) {
					
					foreach ( $json as $val ) {
						$users_to_add = array (
								
								'project_id_fk' => $projectid,
								'user_id_fk' => $val 
						);
						$this->db->insert ( 'project_members', $users_to_add );
					}
				}
				
				$res = $this->getUserDetails ( $projectid );
				$msg = $Sendername . " has created new project";
				$Type = "Project";
				foreach($res as $val)
				{
					$GetDeviceType = $val['device_name'];
					if ($GetDeviceType == 'android') {
						
						$deviceID = $val['reg_id'];
						$this->Android_notification( $json, $Sendername, $userid, $msg, $Type, $deviceID);
					}
					
				    if ($GetDeviceType == 'ios') {
						
						$deviceID = $val['reg_id_ios'];
						$this->Ios_notification($json, $Sendername, $userid, $msg, $Type, $deviceID);
					}
				}
			   
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Success.. project created",
						'users' => $res 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function UploadGalleryPhoto() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'gallery/';
		$config ['allowed_types'] = '*'; // 'mp3|wav';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		
		// print_r($config['file_name']);
		// exit;
		
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'photo_name' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	public function checkUserPoll($poll_id, $user_id) {
		$conditions = array (
				'user_unique_id' => $user_id,
				'poll_id' => $poll_id 
		);
		$this->db->where ( $conditions );
		$res = $this->db->get ( $this->table_poll_votes );
		if ($res->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function totalUsers() {
		$res = $this->db->get ( $this->user_table );
		return $res->num_rows ();
	}
	public function getUserDetails($users) {
		$this->db->where_in ( 'project_id_fk', $users );
		$this->db->select ( 'users.*,users.user_email,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		$this->db->from ( $this->table_project );
		$this->db->join ( $this->user_table, 'users.id =  project_members.user_id_fk' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function delete_group($id) {
		$this->db->where ( 'group_id', $id );
		$query = $this->db->delete ( $this->table );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function checkmembers($projectid, $arr) {
		$this->db->where ( 'project_id_fk', $projectid );
		$this->db->where_in ( 'user_id_fk', $arr );
		$res = $this->db->get ( $this->table_project );
		
		// echo $this->db->last_query(); die();
		$rows = $res->num_rows ();
		
		if ($rows === 0) {
			
			return true;
		}
	}
	public function add_Project_users($arr, $projectid) {
		foreach ( $arr as $val ) {
			$users_to_add = array (
					
					'project_id_fk' => $projectid,
					'user_id_fk' => $val 
			);
			$this->db->insert ( 'project_members', $users_to_add );
		}
		
		$res = $this->getUsersDetails ( $projectid, $arr );
		
		$message = array (
				'status' => true,
				'response_code' => '1',
				'message' => "Members Successfully added",
				'members_added' => $res 
		);
		
		return $message;
	}
	public function getUsersDetails($projectid, $arr) {
		$this->db->where_in ( 'user_id_fk', $arr );
		$this->db->where ( 'project_id_fk', $projectid );
		$this->db->select ( 'users.user_name,users.user_email,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		$this->db->from ( $this->table_project );
		$this->db->join ( $this->user_table, 'users.id =  project_members.user_id_fk' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function RemoveProjectMember($projectid, $userid) {
		$this->db->where ( 'user_id_fk', $userid );
		$this->db->where ( 'project_id_fk', $projectid );
		$query = $this->db->delete ( $this->table_project );
		
		// echo $this->db->last_query(); die();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function checkProjectmembers($projectid, $userid) {
		$this->db->where ( 'project_id_fk', $projectid );
		$this->db->where ( 'user_id_fk', $userid );
		$res = $this->db->get ( $this->table_project );
		
		// echo $this->db->last_query(); die();
		$rows = $res->num_rows ();
		
		if ($rows > 0) {
			
			return $rows;
		}
	}
	public function AddProjectPost($data) {
		$this->db->set ( 'created_date', 'NOW()', FALSE );
		$post = $this->db->insert ( $this->table_post, $data );
		$insert_id = $this->db->insert_id ();
		
		if ($post) {
			
			$res = $this->getPostDetails ( $insert_id );
			
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => "Post Successfully added",
					'post_added' => $res 
			);
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function getPostDetails($insert_id) {
		$this->db->where ( 'post_id', $insert_id );
		$this->db->select ( 'project_post.*,users.user_name,IF(users.user_pic="" ,"null", CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic )) user_pic,count(distinct(activity_likes.like_id)) as likes_count ,count(distinct(activity_comments.comment_id)) as comment_count,IFNULL(activity_likes.is_liked,0) as is_liked ,activity_comments.poster_type' );
		$this->db->from ( $this->table_post );
		$this->db->join ( $this->user_table, 'project_post.user_id_fk = users.id', 'left' );
		$this->db->join ( $this->Likes, 'project_post.post_id = activity_likes.poster_id', 'left' );
		$this->db->join ( $this->Comments, 'project_post.post_id = activity_comments.poster_id', 'left' );
		$this->db->group_by ( 'activity_likes.poster_id' );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	public function UpdateProjectPost($Postid, $data) {
		$this->db->set ( 'modified_date', 'NOW()', FALSE );
		$this->db->where ( 'post_id', $Postid );
		$query = $this->db->update ( $this->table_post, $data );
		
		if ($query) {
			$res = $this->getUpdatedPostDetails ( $Postid );
			
			if ($res) {
				
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Post Successfully Updated",
						'post_added' => $res 
				);
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'ID Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function getUpdatedPostDetails($Postid) {
		$this->db->where ( 'post_id', $Postid );
		$res = $this->db->get ( $this->table_post );
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function UpdateProjects($projectid, $data) {
		$this->db->set ( 'modified_date', 'NOW()', FALSE );
		$this->db->where ( 'project_id', $projectid );
		$query = $this->db->update ( $this->table, $data );
		
		if ($query) {
			$res = $this->getUpdatedProjectDetails ( $projectid );
			
			if ($res) {
				
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Project Successfully Updated",
						'project_id' => $res->project_id,
						'project_title' => $res->project_title,
						'project_desc' => $res->project_desc,
						'photo_name' => base_url ( 'uploads/gallery/' ) . '/' . $res->photo_name,
						'project_admin' => $res->project_admin,
						'created_date' => $res->created_date,
						'modification_date' => $res->modified_date 
				);
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'ID Not Found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Something went wrong' 
			];
		}
		
		return $message;
	}
	public function getUpdatedProjectDetails($projectid) {
		$this->db->where ( 'project_id', $projectid );
		$res = $this->db->get ( $this->table );
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
	public function RemoveProject($ProjectID) {
		$this->db->where ( 'project_id', $ProjectID );
		$query = $this->db->delete ( $this->table );
		
		// echo $this->db->last_query(); die();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function GetAllUsers($userId) {
		$this->db->where_not_in ( 'id', $userId );
		$this->db->where_not_in ( 'id', '1' );
		$res = $this->db->get ( $this->user_table );
		// echo $this->db->last_query(); die();
		
		return $res->result_array ();
	}
	public function GetAllGroups() {
		// $this->db->where( 'group_id_fk', $groupid );
		$this->db->select ( 'groups.*,users.user_name as groupAdminname,COUNT(user_id_fk) as users_count' );
		$this->db->from ( $this->table_group );
		$this->db->join ( $this->table, 'groups.group_id =  group_users.group_id_fk' );
		$this->db->join ( $this->user_table, 'groups.group_admin =  users.id' );
		$this->db->group_by ( 'group_id_fk' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function GetAllProjects() {
		$res = $this->db->get ( $this->table );
		// echo $this->db->last_query(); die();
		
		return $res->result_array ();
	}
	public function GetProjectsByID($userid) {
		$response = array ();
		$this->db->where ( 'user_id_fk', $userid );
		$this->db->select ( 'projects.*,users.user_name as projectAdminname,IF(projects.photo_name="","null",CONCAT("' . base_url ( 'uploads/gallery/' ) . '/",projects.photo_name))photo_name' );
		$this->db->from ( $this->table_project );
		$this->db->join ( $this->table, 'projects.project_id =  project_members.project_id_fk', 'inner' );
		$this->db->join ( $this->user_table, 'projects.project_admin =  users.id', 'inner' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		$data = $res->result_array ();
		
		foreach ( $data as $row ) {
			
			$projectid = $row ['project_id'];
			
			$value = $this->getCount ( $projectid );
			
			foreach ( $value as $val ) {
				$row ['users_count'] = $val ['users_count'];
			}
			
			array_push ( $response, $row );
		}
		
		return $response;
	}
	public function getCount($projectid) {
		$this->db->where ( 'projects.project_id', $projectid );
		$this->db->select ( 'COUNT(user_id_fk) as users_count' );
		$this->db->from ( $this->table_project );
		$this->db->join ( $this->table, 'projects.project_id =  project_members.project_id_fk' );
		$res = $this->db->get ();
		return $res->result_array ();
	}
	public function GetProjectAdmin($ProjectID) {
		$this->db->where ( 'projects.project_id', $ProjectID );
		$this->db->select ( 'projects.project_admin' );
		$this->db->from ( $this->table );
		// $this->db->join ( $this->group, 'groups.group_id = projects.group_id' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->row ()->project_admin;
	}
	public function SeeProjectMembers($projectid) {
		$this->db->where ( 'project_id', $projectid );
		$this->db->select ( 'users.*,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		$this->db->from ( $this->table );
		$this->db->join ( $this->table_project, 'projects.project_id = project_members.project_id_fk' );
		$this->db->join ( $this->user_table, 'project_members.user_id_fk = users.id' );
		$res = $this->db->get ();
		return $res->result ();
		// echo $this->db->last_query(); die();
	}
	public function checkPost($postid) {
		$this->db->where ( 'post_id', $postid );
		$res = $this->db->get ( $this->table_post );
		
		// echo $this->db->last_query(); die();
		$rows = $res->num_rows ();
		
		if ($rows > 0) {
			
			return true;
		}
	}
	public function RemoveProjectPost($projectid, $postid) {
		$this->db->where ( 'post_id', $postid );
		$this->db->where ( 'project_id_fk', $projectid );
		$query = $this->db->delete ( $this->table_post );
		
		// echo $this->db->last_query(); die();
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	public function SeeProjectPosts($projectid, $userid) {
		$response = array ();
		/*
		 * $query = "SELECT `group_post`.`post_id`, `activity_comments`.`body`, `users`.`id`, `users`.`user_name`, `users`.`user_pic`
		 * FROM `group_post`
		 * LEFT JOIN `activity_comments` ON `group_post`.`post_id` =`activity_comments`.`poster_id`
		 * LEFT JOIN `users` ON `activity_comments`.`user_id` =`users`.`id`
		 * WHERE `group_id_fk` = '67'
		 * ORDER BY `creation_date` ASC
		 * LIMIT 4";
		 */
		
		$this->db->where ( 'project_id_fk', $projectid );
		$this->db->select ( 'project_post.*,users.user_name,IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic,count(distinct(activity_likes.like_id)) as likes_count ,count(distinct(activity_comments.comment_id)) as comment_count,activity_likes.poster_id,activity_comments.poster_type' );
		$this->db->from ( $this->table_post );
		$this->db->join ( $this->user_table, 'project_post.user_id_fk = users.id', 'left' );
		$this->db->join ( $this->Likes, 'project_post.post_id = activity_likes.poster_id and activity_likes.type = "Project"', 'left' );
		$this->db->join ( $this->Comments, 'project_post.post_id = activity_comments.poster_id and activity_comments.type = "Project"', 'left' );
		$this->db->group_by ( 'project_post.post_id' );
		$res = $this->db->get ();
		
		// echo $this->db->last_query(); die();
		$result = $res->result_array ();
		// $i =0;
		foreach ( $result as $row ) {
			// print_r($row);
			$postID = $row ['post_id'];
			$posterID = $row ['poster_id'];
			// $row["comments"] = (object) array();
			
			$val = $this->getLikeStatus ( $posterID, $userid );
			
			if ($val) {
				$row ["is_liked"] = '1';
			} 

			else {
				$row ["is_liked"] = '0';
			}
			
			$row ["comments"] = ( object ) ( array ) $this->getAllComments ( $projectid, $postID );
			array_push ( $response, $row );
			// $i++;
			// print_r($response);
		}
		
		return $response;
	}
	public function getAllComments($projectid, $postid) {
		$this->db->limit ( 4 );
		$this->db->order_by ( "creation_date", "asc" );
		$this->db->where ( 'project_id_fk', $projectid );
		$this->db->where ( 'project_post.post_id', $postid );
		$this->db->select ( 'project_post.post_id , activity_comments.comment_id,activity_comments.body,users.id,users.user_name , IF(users.user_pic="" ,"null",CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",users.user_pic ) ) user_pic' );
		// $this->db->select ('activity_comments.body' );
		$this->db->from ( $this->table_post );
		$this->db->join ( $this->Comments, 'project_post.post_id =activity_comments.poster_id and activity_comments.type = "Projects"', 'inner' );
		$this->db->join ( $this->user_table, 'activity_comments.user_id =users.id', 'left' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	public function getLikeStatus($posterID, $userid) {
		$this->db->where ( 'poster_id', $posterID );
		$this->db->where ( 'user_id', $userid );
		$this->db->where ( 'type', 'Project' );
		$res = $this->db->get ( $this->Likes );
		$rows = $res->num_rows ();
		
		if ($rows == 1) {
			
			return true;
		}
	}
	public function updateProjectAdmin($projectid, $value) {
		$this->db->set ( 'modified_date', 'NOW()', FALSE );
		$this->db->where ( 'project_id', $projectid );
		$update = $this->db->update ( $this->table, $value );
		
		if ($update) {
			return true;
		}
	}
	
	public function Android_notification( $json, $Sendername, $userid, $msg, $Type, $deviceID)
	{
		$get = json_decode ( $deviceID );
		
		$obj = (object) $json;
		
		// print_r($get); //die();
		
		/*
		 * $data['data'] = array(
		 * 'type' => 'Poke',
		 *
		 * );
		 */
		// print_r($get);
		
		// $notification = GetNonActionNotificationDetails($action);
		// $message = 'Hi i am sending push notification';
		$api_key = 'AIzaSyDCVGdZc9Abs5cRWPCMWWiKejz9W3hX-wE';
		
		if (empty ( $api_key ) || count ( $get ) < 0) {
			$result = array (
					'success' => '0',
					'message' => 'api or reg id not found'
			);
			echo json_encode ( $result );
			die ();
		}
		$registrationIDs = $get;
		
		$message = $msg;
		$url = 'https://android.googleapis.com/gcm/send';
		$resultData = array (
					"type" => $Type,
					"title" => 'Notification of Project',
					"message" => $message,
					"users" => $obj,
					"poster_id" => $userid,
					
			);
		
	//	print_r($resultData); die();
		
		$fields = array (
				'registration_ids' => $registrationIDs,
				'data' => array (
						"message" => $message,
						"title" => 'Notification of Project',
						"type" => $Type,
						'data' => $resultData
				)
		);
		// print_r($fields); die();
		$headers = array (
				'Authorization: key=' . $api_key,
				'Content-Type: application/json'
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		///print_r ( $result );
	}
	
	public function Ios_notification($json, $Sendername, $userid, $msg, $Type, $deviceID) {
	
	
		//print_r($msg); die();
	
		set_time_limit ( 0 );
	
		// charset header for output
		header ( 'content-type: text/html; charset: utf-8' );
	
		// this is the pass phrase you defined when creating the key
		$passphrase = '123456';
	
		// you can post a variable to this string or edit the message here
		if (! isset ( $_POST ['msg'] )) {
			$_POST ['msg'] = $msg;
		}
	
		// tr_to_utf function needed to fix the Turkish characters
		$message = $_POST ['msg'];
	
	
		//print_r($message); die();
	
		// load your device ids to an array
	
		/*
		 * $item = array (
		 * 'f9c68652e479664827af1c9892e04c2c6b8398561f7e34e3d9f0125f58fc94f1'
		 * );
		 */
	
		// this is where you can customize your notification
	$payload = '{"aps":{
		"alert":"' . $message . '",
		"sound":"default",
		"type": " '.$Type.'",
		"group":"'.$GroupDetails.'"
}}';
		
		$result = 'Start' . '<br />';
	
		// //////////////////////////////////////////////////////////////////////////////
		// start to create connection
		$ctx = stream_context_create ();
		stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'apnsdev.pem' );
		stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
	
		// echo count ( $deviceIds ) . ' devices will receive notifications.<br />';
	
		$get = json_decode ( $deviceID );
	
		// print_r($get); exit;
	
		foreach ( $get as $val ) {
				
			// print_r($val);
			// wait for some time
			sleep ( 1 );
				
			// Open a connection to the APNS server
			$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
				
			if (! $fp) {
				// exit ( "Failed to connect: $err $errstr" . '<br />' );
			} else {
				// echo 'Apple service is online. ' . '<br />';
			}
				
			/*
			 * $payload['aps'] = array(
			 * 'alert' => $message,
			 * 'badge' => $badge,
			 * 'sound' => 'default'
			 * );
			 * $body['data'] = $data;
			 */
				
			// Encode the payload as JSON
			// $payload = json_encode($body);
				
			// Build the binary notification
			$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $val ) . pack ( 'n', strlen ( $payload ) ) . $payload;
				
			// Send it to the server
			$result = fwrite ( $fp, $msg, strlen ( $msg ) );
				
			if (! $result) {
				// echo 'Undelivered message count: ' . $item . '<br />';
			} else {
				//echo $payload;
			}
				
			if ($fp) {
				fclose ( $fp );
				// echo 'The connection has been closed by the client' . '<br />';
			}
		}
	
		// echo count($deviceIds) . ' devices have received notifications.<br />';
	
		// function for fixing Turkish characters
		/*
		 * function tr_to_utf($text) {
		 * $text = trim ( $text );
		 * $search = array (
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�'
		 * );
		 * $replace = array (
		 * 'Ü',
		 * 'Ş',
		 * '&#286;�',
		 * 'Ç',
		 * 'İ',
		 * 'Ö',
		 * 'ü',
		 * 'ş',
		 * 'ğ',
		 * 'ç',
		 * 'ı',
		 * 'ö'
		 * );
		 * $new_text = str_replace ( $search, $replace, $text );
		 * return $new_text;
		 * }
		 */
	
		// set time limit back to a normal value
		set_time_limit ( 30 );
	}
	
}

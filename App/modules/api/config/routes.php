<?php

/*
 * Routing for Users
 */
$route['api/user'] = "User/list";

$route['api/user/signin'] = "User/checkUserAuthentication";
$route['api/user/logout'] = "User/logout";
$route['api/user/invite'] = "User/invite";
$route['api/user/signup'] = "User/registerUser";
$route['api/user/forgotpassword'] = "User/forgotpassword";
$route['api/user/dropUser'] = "User/dropUser";
$route['api/user/updateUser'] = "User/updateUser";
$route['api/user/list'] = "User/userList";




?>


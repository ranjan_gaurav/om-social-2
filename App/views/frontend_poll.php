<?php foreach ($poll_data as $data) { ?>
    <div class="pooling">
        <div class="question"><?php echo $data->title ?></div>
        <div class="pooling-section">

            <?php $options = $this->frontend->getPollOptions($data->poll_id); ?>
            <?php foreach ($options as $option_name) { ?>
                <strong><?php echo ucfirst($option_name->poll_option); ?></strong>
                <div class="progress-bar">
                    <?php
                    $result = ($data->vote_count != '0') ? ($option_name->votes / $data->vote_count ) * 100 : '0';
                    ?>
                    <span style="width:<?php echo round($result); ?>%;"><?php echo $option_name->votes; ?></span>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCG Connect</title>
        <link href="<?php echo base_url(); ?>/theme/css/frontend/stylesheet.css" rel="stylesheet">
        <script>
            var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    <body>
        <div class="content">
            <div class="container">
                <div class="wrapper">
                    <div class="header-items">
                        <img src="<?php echo base_url(); ?>/theme/img/frontend/images/header.png" alt="" width="1000" height="60" usemap="#Map"/>
                        <map name="Map">
                            <area shape="rect" coords="242,5,520,53" href="<?php echo base_url() ?>post/questions">
                            <area shape="rect" coords="526,7,781,54" href="<?php echo base_url() ?>post/polling">
                            <area shape="rect" coords="4,4,232,53" href="<?php echo base_url() ?>post">
                            <area shape="rect" coords="792,7,996,54" href="<?php echo base_url() ?>post/gallery">
                        </map> 
                    </div>


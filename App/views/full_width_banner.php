<?php $this->load->view('includes/header'); ?>
<?php echo modules::run("menu"); ?>
<?php $this->load->view('includes/banner'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php $this->load->view($main_content); ?>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
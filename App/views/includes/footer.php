<footer>
    <div class="container">
        <hr>
        <div class="row">
            <div class="col-md-6 footer-left">
                <ul class="list-inline">
                    <?php foreach (getPages('footer') as $pages) { ?>
                        <li><a href="<?php echo base_url($pages->alias); ?>.html"><?php echo $pages->title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-md-6 footer-right">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-12 footer-below">
                <p><span class="component textComponent footerCopyright" id="textComponent-id22">Developed by <a href="http://orangemantra.com/">O{M}</a> - 2016</span></p>
            </div>
        </div>
    </div>
</footer>
</body>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- PAGE LEVEL FOOTER SCRIPTS -->

<?php if (isset($includes_for_layout_js['js']) AND count($includes_for_layout_js['js']) > 0): ?>
    <?php foreach ($includes_for_layout_js['js'] as $js): ?>
        <?php if ($js['options'] === NULL OR $js['options'] == 'footer'): ?>
            <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
        <?php endif; ?>	
    <?php endforeach; ?>
<?php endif; ?>

<!-- END PAGE LEVEL  FOOTER SCRIPTS -->

<!-- Footer Google Analytics Code -->
<script>
<?php
$analyticsCode = getGoogleAnalyticCode('google_analytic_code');
if ($analyticsCode)
    echo $analyticsCode;
?>
</script>



</html>
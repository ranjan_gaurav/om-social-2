<div class="panel-group RelatedBoards" id="accordion">
    <div class="side-profile">
        <div class="profile-userpic">
            <img src="<?php echo base_url(); ?>/theme/img/admin.jpg" class="img-responsive" alt="">
        </div>

        <div class="profile-usertitle">
            <div class="profile-usertitle-name">
                Welcome Admin
            </div>
        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Users <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse <?php getActiveCollapse('Users'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('Users'); ?>" >
                            <a href="<?php echo base_url('users'); ?>">Users List <span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php /* <div class="panel panel-default">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
      CMS <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
      </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse <?php getActiveCollapse('cms'); ?>">
      <div class="panel-body">
      <table class="table">
      <tr>
      <td class="<?php getActiveMenu('cms'); ?>">
      <a href="<?php echo base_url('cms'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Pages</a>
      </td>
      </tr>
      </table>
      </div>
      </div>
      </div> */ ?>
    


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseNews">
                    Events <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseNews" class="panel-collapse collapse <?php getActiveCollapse('events'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('events'); ?>">
                            <a href="<?php echo base_url('events'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Events</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActivity">
                    Chatters <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseActivity" class="panel-collapse collapse <?php getActiveCollapse('activities'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('activities'); ?>">
                            <a href="<?php echo base_url('activities'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Chatters</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseGall">
                    Gallery <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseGall" class="panel-collapse collapse <?php getActiveCollapse('gallery'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('gallery'); ?>">
                            <a href="<?php echo base_url('gallery'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Gallery</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
       <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseGall1">
                    Groups <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseGall1" class="panel-collapse collapse <?php getActiveCollapse('groups'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('groups'); ?>">
                            <a href="<?php echo base_url('groups'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Groups</a>
                        </td>
                       
                    </tr>
                    
                    
                
                </table>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseQues">
                    Manage Ask Questions <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseQues" class="panel-collapse collapse <?php getActiveCollapse('questions'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('questions'); ?>">
                            <a href="<?php echo base_url('questions'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Questions</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapsePolls">
                    Manage Polls <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapsePolls" class="panel-collapse collapse <?php getActiveCollapse('polls'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('polls'); ?>">
                            <a href="<?php echo base_url('polls'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Polls </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseExhibitors">
                    Manage Exhibitors <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseExhibitors" class="panel-collapse collapse <?php getActiveCollapse('exhibitors'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('exhibitors'); ?>">
                            <a href="<?php echo base_url('exhibitors'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage Exhibitors </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Setting <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse <?php getActiveCollapseSetting('setting'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenuSetting('setting'); ?>">
                            <a href="<?php echo base_url('cms/setting'); ?>"><span class="glyphicon glyphicon-cog text-primary pull-right sub-icon"></span>Setting</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a  href="<?php echo base_url('cms/notification'); ?>">Push Notification <span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span></a>
            </h4>
        </div>
        
    </div>
   
</div>
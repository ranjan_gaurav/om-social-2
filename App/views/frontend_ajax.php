<ul>
    <?php foreach ($lists as $activity_data) { ?>
        <li>
            <?php /*if ($activity_data->photo_id != 'null') { ?>
                <div class="post-content-area">
                    <img src="<?php echo $activity_data->photo_id ?>" alt=""/>
                </div>
            <?php } */?>
            <div class="post-like-section">
                <div class="post-img-section-left">
                    <?php $filename = IMAGESPATH . '/users/profile/' . $activity_data->photo; ?>
                    <?php if ($activity_data->user_pic != 'null' && file_exists($filename) && $activity_data->user_pic != '') { ?>
                        <img src="<?php echo $activity_data->user_pic; ?>" alt=""/>
                    <?php } else { ?>
                        <img src="<?php echo base_url(); ?>/theme/img/default-user.jpg" alt=""/>
                    <?php } ?>
                </div>
                <p>
                    <span><strong><?php echo $activity_data->user_name; ?> : </strong> </span> <br/>
                    <?php echo $activity_data->body; ?> 
                </p> 
            </div>
        </li>
    <?php } ?>

</ul>
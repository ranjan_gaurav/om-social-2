$().ready(function () {
    $("#userForm").validate({
        rules: {
            user_name: "required",
            user_nicename: "required",
            user_email: {
                required: true,
                email: true
            },
            user_pass: {
                required: true,
                minlength: 5
            },
            user_cnf_pass: {
                required: true,
                minlength: 5,
                equalTo: "#user_pass"
            },
            user_role: "required"
        },
        messages: {
            user_nicename: "Please enter HR ID",
            user_email: "Please enter valid email",
        }
    });
})
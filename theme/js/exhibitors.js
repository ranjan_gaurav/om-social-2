$().ready(function () {
    $("#exhibitorsForm").validate({
        rules: {
            exhibitor_name: "required",
            short_description: "required",
            exhibitor_description: "required",
        },
        messages: {
            exhibitor_name: "Please Enter title",
        }
    });

})

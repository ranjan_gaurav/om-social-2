$().ready(function () {
    $("#photoForm").validate({
        rules: {
            title: "required",
            description: "required",
            photo_name: "required",
        },
        messages: {
            title: "Please Enter title",
        }
    });
    $("#photo1Form").validate({
        rules: {
            title: "required",
            description: "required",
        },
        messages: {
            title: "Please Enter title",
        }
    });
})

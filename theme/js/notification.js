$().ready(function () {
    $("#notificationForm").validate({
        rules: {
            title: "required",
            message: "required"
        },
        messages: {
            title: "Please enter Title"
        }
    });
})
